<?php
// fungsi untuk mencetak hitungan dan rincian huruf vokal pada sebuah string
function HitungVokal($str) {
    // deklarasi variabel
    $vocalArr = array();
    // pengecekan setiap huruf vokal
    if (strpos($str, 'a') !== false) {
        array_push($vocalArr, "a");
    }
    if (strpos($str, 'i') !== false) {
        array_push($vocalArr, "i");
    }
    if (strpos($str, 'u') !== false) {
        array_push($vocalArr, "u");
    }
    if (strpos($str, 'e') !== false) {
        array_push($vocalArr, "e");
    }
    if (strpos($str, 'o') !== false) {
        array_push($vocalArr, "o");
    }
    $count = count($vocalArr);
    echo '"' . $str . '" = ' . $count;
    // tidak perlu merinci huruf vokal bila tidak ada
    if ($count == 0) {
        echo "\n";
        return;
    }
    // perincian huruf vokal
    echo " yaitu " . $vocalArr[0];
    if ($count > 1) {
        for ($i = 1; $i < $count; $i++){
            echo " ";
            if ($i == $count - 1) {
                echo "dan ";
            }
            echo $vocalArr[$i];
        }
    }
    echo "\n";
}

// penjalanan test cases
HitungVokal("pt");
HitungVokal("app");
HitungVokal("wisnu");
HitungVokal("bayu");
HitungVokal("manupraba");
HitungVokal("iou");