<?php

namespace App\Http\Controllers;

use App\Exports\EmployeesExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class EmployeeController extends Controller
{
    // fungsi untuk mengembalikan view dan menangani get request
    public function employee(Request $request){
        if ($request->statement == 'insert' && $request->nama != null && is_numeric($request->atasan_id) && is_numeric($request->company_id)) {
            $employee = new Employee;
            $employee->id = DB::table('employee')->find(DB::table('employee')->max('id'))->id + 1;
            $employee->nama = strip_tags($request->nama);
            $employee->atasan_id = $request->atasan_id;
            $employee->company_id = $request->company_id;
            $employee->save();
            session()->flash('message', 'Employee ' . $request->nama . ' berhasil ditambahkan.');
        } elseif ($request->statement == 'update' && is_numeric($request->id)) {
            $employee = Employee::find($request->id);
            if ($request->nama != null) {
                $employee->nama = strip_tags($request->nama);
            }
            if (is_numeric($request->atasan_id)) {
                $employee->atasan_id = $request->atasan_id;
            }
            if (is_numeric($request->company_id)) {
                $employee->company_id = $request->company_id;
            }
            $employee->save();
            session()->flash('message', 'Employee ' . $employee->nama . ' berhasil di-update.');
        } elseif ($request->statement == 'delete' && is_numeric($request->id)) {
            $employee = Employee::find($request->id);
            $employee->delete();
            session()->flash('message', 'Employee dengan ID =' . $request->id . ' berhasil dihapus.');
        }
        $table = "<table><tr><th>ID</th><th>Nama</th><th>Posisi</th><th>ID Atasan</th><th>ID Company</th><th>Perusahaan</th></tr>";
        foreach (Employee::all() as $employee) {
            $table = $table . "<tr><td>" . $employee->id . "</td><td>" .
                $employee->nama . "</td><td>" .
                $this->posisi($employee) . "</td><td>" .
                $employee->atasan_id . "</td><td>" .
                $employee->company_id . "</td><td>" .
                "PT JAVAN" . "</td></tr>";
        }
        $table = $table . "</table>";
        return view('employee', compact('table'));
    }

    // fungsi untuk ekspor excel
    public function excelExport() {
        return Excel::download(new EmployeesExport, 'Employees.xlsx');
    }

    // fungsi untuk ekspor pdf
    public function pdfExport() {
        return Excel::download(new EmployeesExport, 'Employees.pdf');
    }

    // fungsi untuk mengetahui posisi employee
    public function posisi($employee): string
    {
        if ($employee->atasan_id == null) {
            return "CEO";
        } else {
            try {
                $atasan = Employee::find($employee->atasan_id);
                if ($atasan->atasan_id == null) {
                    return "Direktur";
                } elseif (Employee::find($atasan->atasan_id)->atasan_id == null) {
                    return "Manajer";
                } else {
                    return "Staff";
                }
            } catch(Throwable $e) {
                session()->flash('message', 'Error: ' . $e->getMessage());
                return "Bukan CEO";
            }
        }
    }
}
