<?php

namespace App\Http\Controllers;

use App\Exports\CompaniesExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Company;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends Controller
{
    // fungsi untuk mengembalikan view dan menangani request get
    public function company(Request $request){
        if ($request->statement == 'insert' && $request->nama != null && $request->alamat != null) {
            $company = new Company;
            $company->id = DB::table('company')->find(DB::table('company')->max('id'))->id + 1;
            $company->nama = strip_tags($request->nama);
            $company->alamat = strip_tags($request->alamat);
            $company->save();
            session()->flash('message', 'Company ' . $request->nama . ' berhasil ditambahkan.');
        } elseif ($request->statement == 'update' && is_numeric($request->id)) {
            $company = Company::find($request->id);
            if ($request->nama != null) {
                $company->nama = strip_tags($request->nama);
            }
            if ($request->alamat != null) {
                $company->alamat = strip_tags($request->alamat);
            }
            $company->save();
            session()->flash('message', 'Company ' . $company->nama . ' berhasil di-update.');
        } elseif ($request->statement == 'delete' && is_numeric($request->id)) {
            $employee = Company::find($request->id);
            $employee->delete();
            session()->flash('message', 'Company dengan ID =' . $request->id . ' berhasil dihapus.');
        }
        $table = "<table><tr><th>ID</th><th>Nama</th><th>Alamat</th></tr>";
        foreach (Company::all() as $company) {
            $table = $table . "<tr><td>" . $company->id . "</td><td>" .
                $company->nama . "</td><td>" .
                $company->alamat . "</td></tr>";
        }
        $table = $table . "</table>";
        return view('company', compact('table'));
    }

    // fungsi untuk menangani request get ekspor excel Companies
    public function excelExport() {
        return Excel::download(new CompaniesExport, 'Companies.xlsx');
    }

    // fungsi untuk menangani request get ekspor excel Companies
    public function pdfExport() {
        return Excel::download(new CompaniesExport, 'Companies.pdf');
    }
}
