<?php

namespace App\Exports;

use App\Http\Controllers\EmployeeController;
use App\Models\Employee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Throwable;

class EmployeesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(): Collection
    {
        $employees = Employee::all();
        $collection = new Collection([
            ["id", "nama", "Posisi", "Perusahaan"]
        ]);
        foreach ($employees as $employee) {
            $collection = $collection->concat([[$employee->id,
                $employee->nama,
                app(EmployeeController::class)->posisi($employee),
                "PT JAVAN"]]);
        }
        return $collection;
    }
}
