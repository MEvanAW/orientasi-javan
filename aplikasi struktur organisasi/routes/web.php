<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/employee', [EmployeeController::class, 'employee'])->name('employee');
Route::get('/employee/excel-export', [EmployeeController::class, 'excelExport']);
Route::get('/employee/pdf-export', [EmployeeController::class, 'pdfExport']);
Route::get('/company', [CompanyController::class, 'company'])->name('company');
Route::get('/company/excel-export', [CompanyController::class, 'excelExport']);
Route::get('/company/pdf-export', [CompanyController::class, 'pdfExport']);
