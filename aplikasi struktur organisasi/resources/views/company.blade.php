<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Companies</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.min-h-screen{min-height:100vh}.py-4{padding-top:1rem;padding-bottom:1rem}.relative{position:relative}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            body {
                font-family: 'Nunito', sans-serif;
            }
            a {
                text-decoration: none;
                display: inline-block;
                padding: 8px 16px;
            }
            a:hover {
                background-color: #f09697;
                color: black;
            }
            .previous {
                background-color: #f1f1f1;
                color: black;
            }
            .next {
                background-color: #ef3b2d;
                color: #f1f1f1;
            }
            table, th, td {
                color: #f1f1f1;
                border: 1px solid #f1f1f1;
                border-collapse: collapse;
                padding: 5px;
            }
            th {
                background-color: #ef3b2d;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding:35px;">
            <a href="/" class="previous">&laquo; Home</a>
            <a href="/employee" class="next">Employee</a>
            @if (session()->has('message'))
                <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                    <h2 class="text-gray-600 dark:text-gray-400">Notifikasi</h2>
                    <p class="text-gray-600 dark:text-gray-400">{{ session('message') }}</p>
                </div>
            @endif
            <h1 style="color:#EF3B2D;">Companies</h1>
            {!! $table !!}
            <a href="/company/excel-export" class="next" style="margin-top:10px;">Ekspor Excel</a>
            <a href="/company/pdf-export" class="next" style="margin-top:10px;">Ekspor Pdf</a>
            <h2 style="color:#EF3B2D;">Tambahkan Company</h2>
            <form method="get" action="{{route('company')}}">
                <label class="text-gray-600 dark:text-gray-400" for="nama">Nama:</label><br>
                <input type="text" id="nama" value="PT " name="nama" required><br>
                <label class= "text-gray-600 dark:text-gray-400" for="alamat">Alamat:</label><br>
                <input type="text" id="alamat" name="alamat" required><br>
                <button type="submit" value="insert" name="statement">Tambahkan</button>
            </form>
            <h2 style="color:#EF3B2D;">Update Company</h2>
            <p class="text-gray-600 dark:text-gray-400">ID wajib diisi. Bila ada kolom yang kosong, kolom tersebut tidak diubah.</p>
            <form method="get" action="{{route('company')}}">
                <label class="text-gray-600 dark:text-gray-400" for="id">ID:</label><br>
                <input type="number" id="id" value="2" name="id" required><br>
                <label class="text-gray-600 dark:text-gray-400" for="nama">Nama:</label><br>
                <input type="text" id="nama" name="nama"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="alamat">Alamat:</label><br>
                <input type="text" id="alamat" name="alamat"><br>
                <button type="submit" value="update" name="statement">Update</button>
            </form>
            <h2 style="color:#EF3B2D;">Hapus Company</h2>
            <form method="get" action="{{route('company')}}">
                <label class="text-gray-600 dark:text-gray-400" for="id">ID:</label><br>
                <input type="number" id="id" value="2" name="id" required><br>
                <button type="submit" value="delete" name="statement">Hapus</button>
            </form>
        </div>
    </body>
</html>
