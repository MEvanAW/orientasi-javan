<?php
// fungsi untuk mencetak hasil operasi matematika dasar (operand harus berupa whole numbers)
function kalkulatorSederhana($str) {
    // mencetak ulang parameter
    echo $str;
    // menghapus spasi untuk mempermudah pengolahan
    $str = str_replace(" ", "", $str);
    $operandArr = array();
    $operatorArr = array();
    $strlen = strlen($str);
    for ($i = 0; $i < $strlen; $i++) {
        // memasukkan setiap operand ke operandArr
        if (is_numeric($str[$i])) {
            $startIndex = $i;
            while ($i < $strlen - 1) {
                if (!is_numeric($str[$i + 1])) {
                    break;
                }
                else {
                    $i++;
                }
            }
            array_push($operandArr, (int) substr($str, $startIndex, $i - $startIndex + 1));
        }
        // memasukkan setiap operator ke operatorArr
        elseif ($str[$i] == "+" || $str[$i] == "-"|| $str[$i] == "x" || $str[$i] == "/") {
            array_push($operatorArr, $str[$i]);
        }
        // batalkan bila ada karakter tidak dikenali
        else {
            echo ": terdapat karakter tidak dikenali oleh kalkulator: " . $str[$i] . "\n";
            return;
        }
    }
    // batalkan bila jumlah operand dan jumlah operator tidak cocok
    if (count($operandArr) - count($operatorArr) != 1) {
        echo ": banyak operand dan banyak operator tidak cocok.\n";
        return;
    }
    // melakukan perkalian dan/atau pembagian terlebih dahulu
    for($i = 0; $i < count($operatorArr); $i++) {
        if($operatorArr[$i] == "x") {
            $operandArr[$i + 1] = $operandArr[$i] * $operandArr[$i + 1];
            $operandArr[$i] = 0;
            if ($i != 0 && $operatorArr[$i - 1] == "-") {
                $operatorArr[$i] = "-";
            } else {
                $operatorArr[$i] = "+";
            }
        }
        elseif($operatorArr[$i] == "/") {
            if ($operandArr[$i + 1] == 0) {
                echo ": pembagian dengan nol adalah tak terdefinisi.\n";
                return;
            }
            $operandArr[$i + 1] = $operandArr[$i] / $operandArr[$i + 1];
            $operandArr[$i] = 0;
            if ($i != 0 && $operatorArr[$i - 1] == "-") {
                $operatorArr[$i] = "-";
            } else {
                $operatorArr[$i] = "+";
            }
        }
    }
    // melakukan penjumlahan dan/atau pengurangan
    for($i = 0; $i < count($operatorArr); $i++) {
        if($operatorArr[$i] == "+") {
            $operandArr[$i + 1] = $operandArr[$i] + $operandArr[$i + 1];
        }
        else {
            $operandArr[$i + 1] = $operandArr[$i] - $operandArr[$i + 1];
        }
    }
    // mencetak hasil
    echo " = " . end($operandArr) . "\n";
}
kalkulatorSederhana("2 + 2");
kalkulatorSederhana("3 - 1");
kalkulatorSederhana("4 x 4");
kalkulatorSederhana("2 / 2");
kalkulatorSederhana("12 + 23 - 34 x 45 / 56");
kalkulatorSederhana("123 x 234 - 345 + 456 / 567");
kalkulatorSederhana("2 / 0");
kalkulatorSederhana("dummy");
kalkulatorSederhana("1 + 1 + ");