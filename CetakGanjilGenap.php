<?php
// fungsi untuk menentukan apakah sebuah angka genap atau tidak
function isEven($num) {
    return $num%2 == 0;
}

// fungsi untuk mencetak kalimat apakah masing-masing dari barisan angka ganjil atau genap
function cetakGanjilGenap($A, $B) {
    $str = "genap";
    if (!isEven($A)){
        $str = "ganjil";
    }
    for ($i = $A; $i <= $B; $i++) {
        echo "Angka " . $i . " adalah " . $str . "\n";
        if ($str == "genap") {
            $str = "ganjil";
        } else {
            $str = "genap";
        }
    }
}

// penjalanan test cases
cetakGanjilGenap(1,4);
cetakGanjilGenap(10,11);
cetakGanjilGenap(6,8);