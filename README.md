# Proyek Orientasi Javan

Ini adalah proyek orientasi saya pada magang di PT. Javan Cipta Solusi.

## Tugas yang Sudah Selesai

1. Tugas 0 seluruhnya (terdapat pada elearning.javanlabs.com)
2. Tugas 1: Basic Programming
    * Cetak Ganjil Genap
    * Hitung Vokal
    * Kalkulator Sederhana
3. Tugas 2: Model View Controller Menggunakan Framework
    * Setup Hello World Menggunakan Framework Laravel
    * Mengubah Kode Kalkulator ke MVC
    * Mengubah Kode Ganjil Genap ke MVC
    * Mengubah Kode Hitung Huruf Vokal ke MVC
4. Tugas 3: SQL Insert, Update, Delete, dan Kasus Struktur Organisasi
    * Insert SQL menggunakan DataGrip
    * Update SQL di DataGrip
    * Delete SQL
    * Kasus Struktur Organisasi (1)
    * Kasus Struktur Organisasi (2)
5. Tugas 4: CRUD Struktur Organisasi
    * CRUD Employee
    * CRUD Company
    * Tree: Kasus Keluarga Kerajaan
    * Aplikasi One to Many dan Many to Many Menggunakan Laravel
6. Tugas 5: Export Data ke PDF dan Excel
    * Export Data ke PDF
    * Export Data ke Excel
7. Tugas 6: Advanced SQL
    * Membuat Query Customer Complaints
    * Data Analytics SQL
    * Membuat Tampilan Statistik Menggunakan Google Chart
8. Tugas 7: Data Relationship
    * Sistem Informasi Kampus
9. Tugas 8: Basic Git dan Setup Solid Environment
    * Branching
    * Commit
    * Merge Request
    * Setup Solid Env
10. Tugas 8: Laravolt
    * Instalasi Laravolt
    * CRUD dasar menggunakan Laravolt
    * Implementasi Laravolt pada Aplikasi Sistem Informasi Kampus

## Kontak Pengembang

Nomor terhubung Telegram: 081329320383

Atau kirim email ke saya: [evananindya@mail.ugm.ac.id](mailto:evananindya@mail.ugm.ac.id)

## Screenshots Situs Web Hasil Laravel (Tugas 2)

Halaman Home (Hello World)

![Hello World Laravel](markdown-images/hello-world.jpg)

Halaman Kalkulator Sederhana

![Kalkulator Sederhana Laravel](markdown-images/kalkulator-sederhana.jpg)

Halaman Hasil (Kalkulator Sederhana)

![Hasil Kalkulator Sederhana Laravel](markdown-images/hasil-kalkulator-sederhana.jpg)

Halaman Cetak Ganjil Genap

![Cetak Ganjil Genap](markdown-images/cetak-ganjil-genap.jpg)

Halaman Hasil (Cetak Ganjil Genap)

![Hasil Cetak Ganjil Genap](markdown-images/hasil-cetak-ganjil-genap.jpg)

Halaman Penghitung Huruf Vokal

![Penghitung Huruf Vokal](markdown-images/hitung-vokal.jpg)

Halaman Hasil (Hitung Huruf Vokal)

![Hasil Hitung Huruf Vokal](markdown-images/hasil-hitung-vokal.jpg)
