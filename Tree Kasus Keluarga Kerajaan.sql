CREATE TABLE `kerajaan` (
  `id` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kelamin` varchar(100) DEFAULT NULL,
  `parent_id` varchar(100) DEFAULT NULL
);
INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
	 ('E001','Elizabeth II','female',NULL),
	 ('E002','Charles','male','E001'),
	 ('E003','Anne','female','E001'),
	 ('E004','Andrew','male','E001'),
	 ('E005','Edward','male','E001'),
	 ('E006','William','male','E002'),
	 ('E007','Harry','male','E002'),
	 ('E008','Zara','female','E003'),
	 ('E009','Peter','male','E003'),
	 ('E010','Eugenie','female','E004');
INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
	 ('E011','Beatrice','female','E004'),
	 ('E012','James','male','E005'),
	 ('E013','Louise','female','E005'),
	 ('E014','George','male','E006'),
	 ('E015','Charlotte','male','E006'),
	 ('E016','Louis','male','E006'),
	 ('E017','Lena','female','E008'),
	 ('E018','Mia','female','E008'),
	 ('E019','Isla','female','E009'),
	 ('E020','Savannah','female','E009');
INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
	 ('M001','Mpu Sindok','male',NULL),
	 ('M002','Sri Isanatunggawijaya','male','M001'),
	 ('M003','Sri Makutawangsawardhana','male','M002'),
	 ('M004','Sri Dharmawangsa','male','M003'),
	 ('M005','Gunapriadharmptani','female','M003'),
	 ('M006','Airlangga','male','M005'),
	 ('M007','Marakata','male','M005'),
	 ('M008','Anak Wungsu','male','M005');