CREATE OR REPLACE VIEW console_game_titles AS
    SELECT Name, Platform, Year
        FROM console_games
        ORDER BY Platform ASC, Year DESC;