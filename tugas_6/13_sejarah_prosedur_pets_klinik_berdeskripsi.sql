SELECT ph.PetID, Date, ph.ProcedureType, Description
    FROM procedures_history AS ph INNER JOIN procedures_details AS pd
        ON ph.ProcedureType = pd.ProcedureType AND ph.ProcedureSubCode = pd.ProcedureSubCode
        INNER JOIN pets ON pets.PetID = ph.PetID;