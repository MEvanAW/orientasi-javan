SELECT o.OwnerID, o.Name AS `Owner Name`, Surname, Price, Date, p.Name  AS `Pet Name`, ph.ProcedureType, Description
    FROM procedures_history AS ph INNER JOIN pets AS p ON ph.PetID = p.PetID
        INNER JOIN owners AS o ON o.OwnerID = p.OwnerID
        INNER JOIN procedures_details AS pd ON pd.ProcedureType = ph.ProcedureType AND pd.ProcedureSubCode = ph.ProcedureSubCode
    ORDER BY o.OwnerId;