SELECT Platform,
    CASE -- If Discontinued is null, it is assumed that the platform still available
        WHEN Discontinued is not null THEN DATEDIFF(Discontinued, FirstRetailAvailability)
        ELSE DATEDIFF(CURDATE(), FirstRetailAvailability)
    END AS `Longevity (Days)`
    FROM console_dates
    ORDER BY `Longevity (Days)`;