CREATE OR REPLACE VIEW company_response AS
    SELECT a.Company,
        COUNT(b.`Company Response to Consumer`) AS `Explanation`,
        COUNT(c.`Company Response to Consumer`) AS `Monetary relief`,
        COUNT(d.`Company Response to Consumer`) AS `Non-monetary relief`,
        COUNT(e.`Company Response to Consumer`)  AS `Closed`,
        COUNT(f.`Company Response to Consumer`) AS `Untimely response`
        FROM customer_complaints AS a LEFT JOIN customer_complaints as b ON a.id = b.id AND b.`Company Response to Consumer` = 'Closed with explanation'
            LEFT JOIN customer_complaints AS c ON a.id = c.id AND c.`Company Response to Consumer` = 'Closed with monetary relief'
            LEFT JOIN customer_complaints AS d ON a.id = d.id AND d.`Company Response to Consumer` = 'Closed with non-monetary relief'
            LEFT JOIN customer_complaints AS e ON a.id = e.id AND e.`Company Response to Consumer` = 'Closed'
            LEFT JOIN customer_complaints AS f ON a.id = f.id AND f.`Company Response to Consumer` = 'Untimely response'
        GROUP BY a.Company
        ORDER BY Company;