SELECT PetID, pets.Name AS `Pet Name`, owners.Name AS `Owner Name`, owners.Surname AS `Owner Surname`, owners.OwnerID
        FROM pets INNER JOIN owners ON pets.OwnerID = owners.OwnerID;S