SELECT Platform, FirstRetailAvailability AS `Release Date`,
    CASE
        WHEN Month(FirstRetailAvailability) = 11 THEN 'Black Friday'
        WHEN Month(FirstRetailAvailability) = 12 THEN 'Christmas'
    END AS `Just Before`
    FROM console_dates
    WHERE
        ( -- Check if it is November
            MONTH(FirstRetailAvailability) = 11
            AND ( -- Check if within a week before Black Friday (Black Friday not included)
                -- Friday, Saturday, and Sunday the week before
                (WEEK(FirstRetailAvailability) - WEEK(DATE_FORMAT(FirstRetailAvailability, '%Y-%m-01')) = 2
                    AND WEEKDAY(FirstRetailAvailability) >= 4)
                -- Monday till Thursday the week
                OR (WEEK(FirstRetailAvailability) - WEEK(DATE_FORMAT(FirstRetailAvailability, '%Y-%m-01')) = 3
                    AND WEEKDAY(FirstRetailAvailability) < 4)
            )
        )
        OR ( -- Check if it is December
            MONTH(FirstRetailAvailability) = 12
            -- Check if within a week before Christmas (Christmas not included)
            AND 25 - DAY(FirstRetailAvailability) < 7
        );