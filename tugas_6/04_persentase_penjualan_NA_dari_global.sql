SELECT SUM(NA_Sales) / (SUM(NA_Sales)+SUM(EU_Sales)+SUM(JP_Sales)+SUM(Other_Sales)) * 100
    AS `NA Sales Percentage of Global Sales`
    FROM console_games;