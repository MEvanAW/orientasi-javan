SELECT DISTINCT(ph.PetID), Name AS `Pets had procedures performed`, Kind, Gender, Age, OwnerID
    FROM pets INNER JOIN procedures_history AS ph on pets.PetID = ph.PetID;