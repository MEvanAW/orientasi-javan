SELECT MONTH(`Date Received`) as Bulan, YEAR(`Date Received`) as Tahun, count(id) as `Banyak Komplain`
    from customer_complaints
    group by MONTH(`Date Received`), YEAR(`Date Received`);