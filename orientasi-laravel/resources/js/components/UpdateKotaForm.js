import { useState } from "react";
import ReactDOM from 'react-dom';
import './App.css';

// mengembalikan elemen root div berisi form Create Kota
function UpdateKotaForm(props) {
    const [id, setId] = useState("");
    const [nama, setNama] = useState("");
    const [luas, setLuas] = useState("");
    const [penduduk, setPenduduk] = useState("");
    const [provinsiId, setProvinsiId] = useState("");

    const jsonProvinces = JSON.parse(props.provinces);
    const provinsiLabel = <div>
        <label>Provinsi<sup>*</sup>:</label>
        <br/>
    </div>
    const options = Object.keys(jsonProvinces).map((id) =>
        <option value={id}>{jsonProvinces[id]}</option>);
    const select = <div>
        <select value={provinsiId}
                onChange={(e) => setProvinsiId(e.target.value)}
                name="provinsi_id"
                id="provinsi_id">
            {options}
        </select>
        <br/>
    </div>

    return (
        <div>
            <h2>Perbarui Kota/Kabupaten</h2>
            <p>Gunakan form ini untuk memperbarui kota/kabupaten.</p>
            <form method="get" action="/kota">
                <label>ID Kota/Kabupaten<sup>*</sup></label><br/>
                <input
                    type="number"
                    value={id}
                    id="id"
                    name="id"
                    required={true}
                    onChange={(e) => setId(e.target.value)}/><br/>
                <label>Nama Kota/Kabupaten:</label><br/>
                <input
                    type="text"
                    value={nama}
                    id="nama"
                    name="nama"
                    onChange={(e) => setNama(e.target.value)}/><br/>
                <label>Luas Kota/Kabupaten (km<sup>2</sup>):</label><br/>
                <input
                    type="number"
                    value={luas}
                    id="luas"
                    name="luas"
                    onChange={(e) => setLuas(e.target.value)}/><br/>
                <label>Jumlah Penduduk Kota/Kabupaten (jiwa):</label><br/>
                <input
                    type="number"
                    value={penduduk}
                    id="penduduk"
                    name="penduduk"
                    onChange={(e) => setPenduduk(e.target.value)}/><br/>
                {provinsiLabel}
                {select}
                <button type="submit" value="update" name="statement">Perbarui</button>
            </form>
            <p>*) wajib diisi.</p>
        </div>
    );
}

export default UpdateKotaForm;

// DOM element
if (document.getElementById('update-kota')) {
    const element = document.getElementById('update-kota');
    const props = Object.assign({}, element.dataset);
    ReactDOM.render(<UpdateKotaForm {...props}/>, element);
}
