import { useState } from "react";
import ReactDOM from 'react-dom';
import './App.css';

function CreateProvinsiForm() {
    const [nama, setNama] = useState("");
    const [luas, setLuas] = useState("");
    const [penduduk, setPenduduk] = useState("");

    return (
        <div>
            <h2>Tambahkan Provinsi</h2>
            <p>Gunakan form ini untuk menambahkan provinsi. Ketiga field wajib diisi.</p>
            <form method="get" action="/provinsi">
                <label>Nama Provinsi:</label><br/>
                <input
                    type="text"
                    value={nama}
                    id="nama"
                    name="nama"
                    required={true}
                    onChange={(e) => setNama(e.target.value)}/><br/>
                <label>Luas Provinsi (km<sup>2</sup>):</label><br/>
                <input
                    type="number"
                    value={luas}
                    id="luas"
                    name="luas"
                    required={true}
                    onChange={(e) => setLuas(e.target.value)}/><br/>
                <label>Jumlah Penduduk (jiwa):</label><br/>
                <input
                    type="number"
                    value={penduduk}
                    id="penduduk"
                    name="penduduk"
                    required={true}
                    onChange={(e) => setPenduduk(e.target.value)}/><br/>
                <button type="submit" value="create" name="statement">Tambahkan</button>
            </form>
        </div>
    );
}

export default CreateProvinsiForm;

// DOM element
if (document.getElementById('create-provinsi')) {
    ReactDOM.render(<CreateProvinsiForm />, document.getElementById('create-provinsi'));
}
