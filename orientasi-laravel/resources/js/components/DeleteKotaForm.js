import { useState } from "react";
import ReactDOM from 'react-dom';
import './App.css';

function DeleteKotaForm() {
    const [id, setId] = useState("");

    return (
        <div>
            <h2>Hapus Kota/Kabupaten</h2>
            <p>Gunakan form ini untuk menghapus kota/kabupaten. ID wajib diisi.</p>
            <form method="get" action="/kota">
                <label>ID Provinsi:</label><br/>
                <input
                    type="number"
                    value={id}
                    id="id"
                    name="id"
                    required={true}
                    onChange={(e) => setId(e.target.value)}/><br/>
                <button type="submit" value="delete" name="statement">Hapus</button>
            </form>
        </div>
    );
}

export default DeleteKotaForm;

// DOM element
if (document.getElementById('delete-kota')) {
    ReactDOM.render(<DeleteKotaForm />, document.getElementById('delete-kota'));
}
