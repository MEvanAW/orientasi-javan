import { useState } from "react";
import ReactDOM from 'react-dom';
import './App.css';

function DeleteProvinsiForm() {
    const [id, setId] = useState("");

    return (
        <div>
            <h2>Hapus Provinsi</h2>
            <p>Gunakan form ini untuk menghapus provinsi. ID wajib diisi.</p>
            <form method="get" action="/provinsi">
                <label>ID Provinsi:</label><br/>
                <input
                    type="number"
                    value={id}
                    id="id"
                    name="id"
                    required={true}
                    onChange={(e) => setId(e.target.value)}/><br/>
                <button type="submit" value="delete" name="statement">Hapus</button>
            </form>
        </div>
    );
}

export default DeleteProvinsiForm;

// DOM element
if (document.getElementById('delete-provinsi')) {
    ReactDOM.render(<DeleteProvinsiForm />, document.getElementById('delete-provinsi'));
}
