import { useState } from "react";
import ReactDOM from 'react-dom';
import './App.css';

function UpdateProvinsiForm() {
    const [id, setId] = useState("");
    const [nama, setNama] = useState("");
    const [luas, setLuas] = useState("");
    const [penduduk, setPenduduk] = useState("");

    return (
        <div>
            <h2>Perbarui Provinsi</h2>
            <p>Gunakan form ini untuk memperbarui provinsi. Field ID wajib diisi. Bila field selain ID kosong, dianggap bahwa tidak ada perubahan pada field tersebut.</p>
            <form method="get" action="/provinsi">
                <label>ID Provinsi:</label><br/>
                <input
                    type="number"
                    value={id}
                    id="id"
                    name="id"
                    required={true}
                    onChange={(e) => setId(e.target.value)}/><br/>
                <label>Nama Provinsi:</label><br/>
                <input
                    type="text"
                    value={nama}
                    id="nama"
                    name="nama"
                    onChange={(e) => setNama(e.target.value)}/><br/>
                <label>Luas Provinsi (km<sup>2</sup>):</label><br/>
                <input
                    type="number"
                    value={luas}
                    id="luas"
                    name="luas"
                    onChange={(e) => setLuas(e.target.value)}/><br/>
                <label>Jumlah Penduduk (jiwa):</label><br/>
                <input
                    type="number"
                    value={penduduk}
                    id="penduduk"
                    name="penduduk"
                    onChange={(e) => setPenduduk(e.target.value)}/><br/>
                <button type="submit" value="update" name="statement">Perbarui</button>
            </form>
        </div>
    );
}

export default UpdateProvinsiForm;

// DOM element
if (document.getElementById('update-provinsi')) {
    ReactDOM.render(<UpdateProvinsiForm />, document.getElementById('update-provinsi'));
}
