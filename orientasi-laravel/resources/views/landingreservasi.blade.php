<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reservasi Film</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Rel -->
        <link rel="stylesheet" href="https://21cineplex.com//theme/v5/assets/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="https://21cineplex.com//theme/v5/assets/css/style.css?as=1633503394" type="text/css" />

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.min-h-screen{min-height:100vh}.py-4{padding-top:1rem;padding-bottom:1rem}.relative{position:relative}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            .button {
                text-decoration: none;
                display: inline-block;
                padding: 8px 16px;
            }
            .button:hover {
                background-color: #146a65;
            }
            .previous {
                background-color: #0d7e40;
            }
            .next {
                background-color: #1B578A;
                color: #f1f1f1;
            }
            input {
                border: 1px solid #0d7e40;
            }
            .column {
                float: left;
                width: 33.33%;
                padding: 10px;
            }
            .row:after {
                content: "";
                display: table;
                clear: both;
            }
            @media screen and (max-width: 600px) {
                .column {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container clearfix">
            <!-- Tautan untuk ke Tugas Lain -->
            <a href="/" class="button previous">&laquo; Home</a>
            <a href="/ganjilgenap" class="button next">Cetak Ganjil Genap</a>
            <a href="/hitungvokal" class="button next">Penghitung Huruf Vokal</a>
            <a href="/provinsi" class="button next">CRUD Data Daerah</a>

            <!-- Judul Halaman -->
            <h1 style="color:#1B578A;">Reservasi Film</h1>

            <!-- Tombol untuk ke Halaman Penonton Film-->
            <a href="/penonton-film" class="button next">Daftar Pelanggan</a>

            <!-- Menampilkan Error Jika Ada-->
            @if ($errors->any())
                <div  style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #1B578A;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #ffffff;">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Menampilkan Pesan Jika Ada-->
            @if (session()->has('message'))
                <div style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #1B578A;">
                    <h2 style="color: #ffffff;">Notifikasi</h2>
                    <p style="color: #ffffff;">{{ session('message') }}</p>
                </div>
            @endif

            <!-- Film Tersedia -->
            <div id="now-playing" class="row clearfix">
                @foreach ($films as $film)
                    <div class="col-3">
                        <div class="movie">
                            <a href="/film?judul={{$film->nama}}">
                                <div class="movie-poster">
                                    <img src="{{$film->tautan_poster}}" alt="{{$film->nama}}" width="290" height="426"/>
                                </div>
                                <div class="movie-desc">
                                    <h4>{{$film->nama}}</h4>
                                    <span class="movie-label"><img src="https://21cineplex.com//theme/v5/assets/img/icons/labels/{{$film->rating_usia}}.png" alt="{{$film->rating_usia}}" /></span>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

            <!-- Div Penampung Tiga Form -->
            <div class="row">
                <!-- Form Create Film -->
                <div class="column">
                    <h2 style="color:#1B578A;">Tambahkan Film</h2>
                    <p>Gunakan form ini untuk menambahkan film.</p>
                    <form method="post" action="{{route('create-update-film')}}">
                        @csrf
                        <label for="nama">Judul Film<sup>*</sup>:</label><br>
                        <input type="text" id="nama" name="nama" required><br>
                        <label for="tautan_poster">Tautan Poster<sup>*</sup>:</label><br>
                        <input type="text" id="tautan_poster" name="tautan_poster" required><br>
                        <label for="rating_usia">Rating Usia<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="rating_usia" id="rating_usia">
                                <option value="su">Semua Usia (su)</option>
                                <option value="r13">Remaja (r13+)</option>
                                <option value="d17">Dewasa (d17+)</option>
                            </select><br>
                        </border>
                        <button type="submit" value="create" name="statement">Tambahkan</button>
                    </form>
                    <p>*) wajib diisi</p>
                </div>

                <!-- Form Update Film -->
                <div class="column">
                    <h2 style="color:#1B578A;">Perbarui Film</h2>
                    <p>Gunakan form ini untuk memperbarui film. Field tidak wajib yang dikosongkan akan dianggap sama dengan sebelumnya.</p>
                    <form method="post" action="{{route('create-update-film')}}">
                        @csrf
                        <label for="id">Judul Film<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="id" id="id">
                                @foreach ($films as $film)
                                    <option value="{{$film->id}}">{{$film->nama}}</option>
                                @endforeach
                            </select><br>
                        </border>
                        <label for="nama">Ganti Judul Menjadi:</label><br>
                        <input type="text" id="nama" name="nama"><br>
                        <label for="tautan_poster">Tautan Poster:</label><br>
                        <input type="text" id="tautan_poster" name="tautan_poster"><br>
                        <label for="rating_usia">Rating Usia<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="rating_usia" id="rating_usia">
                                <option value="su">Semua Usia (su)</option>
                                <option value="r13">Remaja (r13+)</option>
                                <option value="d17">Dewasa (d17+)</option>
                            </select><br>
                        </border>
                        <button type="submit" value="update" name="statement">Perbarui</button>
                    </form>
                    <p>*) wajib diisi</p>
                </div>

                <!-- Form Delete Film -->
                <div class="column">
                    <h2 style="color:#1B578A;">Hapus Film</h2>
                    <p>Gunakan form ini untuk menghapus film.</p>
                    <form method="post" action="{{route('delete-film')}}">
                        @csrf
                        <label for="nama">Judul Film<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="id" id="id">
                                @foreach ($films as $film)
                                    <option value="{{$film->id}}">{{$film->nama}}</option>
                                @endforeach
                            </select><br>
                        </border>
                        <button type="submit">Hapus</button>
                    </form>
                    <p>*) wajib diisi</p>
                </div>
            </div>
        </div>
    </body>
</html>
