<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CRUD Data Kecamatan</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.min-h-screen{min-height:100vh}.py-4{padding-top:1rem;padding-bottom:1rem}.relative{position:relative}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            body {
                font-family: 'Nunito', sans-serif;
            }
            a {
                text-decoration: underline;
                color: #f1f1f1;
            }
            .button {
                text-decoration: none;
                display: inline-block;
                padding: 8px 16px;
            }
            .button:hover {
                background-color: #f09697;
                color: black;
            }
            .previous {
                background-color: #f1f1f1;
                color: black;
            }
            .next {
                background-color: #ef3b2d;
                color: #f1f1f1;
            }
            table, th, td {
                color: #f1f1f1;
                border: 1px solid #f1f1f1;
                border-collapse: collapse;
                padding: 5px;
            }
            th {
                background-color: #ef3b2d;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding:35px;">
            <!-- Tombol ke Halaman Lain -->
            <a href="/kota" class="button previous">&laquo; Kota</a>
            <a href="/kalkulator" class="button next">Kalkulator Sederhana</a>
            <a href="/ganjilgenap" class="button next">Cetak Ganjil Genap</a>
            <a href="/hitungvokal" class="button next">Penghitung Huruf Vokal</a>

            <!-- Menampilkan Error Jika Ada-->
            @if ($errors->any())
                <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #f1f1f1;">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Menampilkan Pesan Jika Ada-->
            @if (session()->has('message'))
                <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                    <h2 style="color: #f1f1f1;">Notifikasi</h2>
                    <p style="color: #f1f1f1;">{{ session('message') }}</p>
                </div>
            @endif
            {!! $h1 !!}
            @if ($namaKota == "")
                <p class="text-gray-600 dark:text-gray-400">Ini digunakan untuk menulis, membaca, memperbarui, dan menghapus data Kecamatan.</p>
            @else
                <p class="text-gray-600 dark:text-gray-400">Ini digunakan untuk menulis, membaca, memperbarui, dan menghapus data Kecamatan di {{$namaKota}}.</p>
            @endif
            {!! $table !!}
            @if ($namaKota != "")
                <a href="/kecamatan">Tampilkan Semua Kecamatan</a>
            @endif

            <!-- Form Create Kecamatan -->
            <h2 style="color:#ef3b2d;">Tambahkan Kecamatan</h2>
            <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan kecamatan.</p>
            <form method="post" action="{{route('crud-kecamatan')}}">
                @csrf
                <label class="text-gray-600 dark:text-gray-400" for="nama">Nama Kecamatan<sup>*</sup>:</label><br>
                <input type="text" id="nama" name="nama" required><br>
                <label class= "text-gray-600 dark:text-gray-400" for="luas">Luas Kecamatan<sup>*</sup>:</label><br>
                <input type="number" id="luas" name="luas" required><br>
                <label class= "text-gray-600 dark:text-gray-400" for="penduduk">Penduduk<sup>*</sup>:</label><br>
                <input type="number" id="penduduk" name="penduduk" required><br>
                <label class= "text-gray-600 dark:text-gray-400" for="kota_id">Kota/Kabupaten<sup>*</sup>:</label><br>
                <select name="kota_id" id="kota_id">
                    @foreach ($cities as $city)
                        <option value="{{$city->id}}">{{$city->nama}}</option>
                    @endforeach
                </select><br>
                <button type="submit" value="create" name="statement">Tambahkan</button>
            </form>
            <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

            <!-- Form Update Kecamatan -->
            <h2 style="color:#EF3B2D;">Perbarui Kecamatan</h2>
            <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui kecamatan. Field yang tidak wajib dianggap sama dengan sebelumnya apabila tidak diisi.</p>
            <form method="post" action="{{route('crud-kecamatan')}}">
                @csrf
                <label class="text-gray-600 dark:text-gray-400" for="id">ID Kecamatan<sup>*</sup>:</label><br>
                <input type="number" id="id" name="id" required><br>
                <label class="text-gray-600 dark:text-gray-400" for="nama">Nama Kecamatan:</label><br>
                <input type="text" id="nama" name="nama"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="luas">Luas Kecamatan:</label><br>
                <input type="number" id="luas" name="luas"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="penduduk">Jumlah Penduduk:</label><br>
                <input type="number" id="penduduk" name="penduduk"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="kota_id">Kota/Kabupaten<sup>*</sup>:</label><br>
                <select name="kota_id" id="kota_id">
                    @foreach ($cities as $city)
                        <option value="{{$city->id}}">{{$city->nama}}</option>
                    @endforeach
                </select><br>
                <button type="submit" value="update" name="statement">Perbarui</button>
            </form>
            <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

            <!-- Form Delete Kecamatan -->
            <h2 style="color:#EF3B2D;">Hapus Kecamatan</h2>
            <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus kecamatan.</p>
            <form method="post" action="{{route('crud-kecamatan')}}">
                @csrf
                <label class="text-gray-600 dark:text-gray-400" for="id">ID Kecamatan<sup>*</sup>:</label><br>
                <input type="number" id="id" name="id" required><br>
                <button type="submit" value="delete" name="statement">Hapus</button>
            </form>
            <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
        </div>
    </body>
</html>
