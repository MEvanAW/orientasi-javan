<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Kalkulator Sederhana</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.min-h-screen{min-height:100vh}.py-4{padding-top:1rem;padding-bottom:1rem}.relative{position:relative}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            body {
                font-family: 'Nunito', sans-serif;
            }
            a {
                text-decoration: none;
                display: inline-block;
                padding: 8px 16px;
            }
            a:hover {
                background-color: #f09697;
                color: black;
            }
            .previous {
                background-color: #f1f1f1;
                color: black;
            }
            .next {
                background-color: #ef3b2d;
                color: #f1f1f1;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding:35px">
            <a href="/" class="previous">&laquo; Home</a>
            <a href="/ganjilgenap" class="next">Cetak Ganjil Genap</a>
            <a href="/hitungvokal" class="next">Penghitung Huruf Vokal</a>
            <a href="/provinsi" class="next">CRUD Data Daerah</a>
            <h1 style="color:#ef3b2d;">Kalkulator Sederhana</h1>
            <p class="text-gray-600 dark:text-gray-400">Kalkulator sederhana ini dapat digunakan untuk melakukan operasi matematika sederhana: penambahan, pengurangan, perkalian, dan/atau pembagian.</p>
            <h2 class="text-gray-600 dark:text-gray-400">Dua Operand</h2>
            <p class="text-gray-600 dark:text-gray-400">Bagian ini terdapat dua operand. Masukkan nilai Operand 1 dan nilai Operand 2, kemudian tekan +, -, x, atau / untuk melakukan operasi matematika terkait.</p>
            <form method="get" action="{{route('hasil')}}">
                <label class="text-gray-600 dark:text-gray-400" for="operand1">Operand 1:</label>
                <input type="number" id="operand1" value="1" name="operand1" required><br>
                <label class= "text-gray-600 dark:text-gray-400" for="operand2">Operand 2:</label>
                <input type="number" id="operand2" value="2" name="operand2" required><br>
                <button type="submit" value="+" name="operator">+</button> <button value="-" name="operator">-</button> <button value="x" name="operator">x</button> <button value="/" name="operator">/</button>
            </form>
            <h2 class= "text-gray-600 dark:text-gray-400">Banyak Operand Bebas</h2>
            <p class= "text-gray-600 dark:text-gray-400">Bagian ini dapat menampung operand berapapun. Ketikkan pernyataan matematis, kemudian tekan hitung.</p>
            <form method="get" action="{{route('hasil')}}">
                <input type="text" id="str" value="1 - 2 + 3 x 4 / 5" name="str" required><br>
                <button type="submit">hitung</button>
            </form>
        </div>
    </body>
</html>
