<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Film</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.min-h-screen{min-height:100vh}.py-4{padding-top:1rem;padding-bottom:1rem}.relative{position:relative}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            body {
                font-family: 'Nunito', sans-serif;
            }
            .button {
                text-decoration: none;
                display: inline-block;
                padding: 8px 16px;
            }
            .button:hover {
                background-color: #146a65;
            }
            .previous {
                background-color: #0d7e40;
                color: #ffffff;
            }
            .next {
                background-color: #1B578A;
                color: #ffffff;
            }
            .submit {
                margin-top: 8px;
            }
            input {
                border: 1px solid #0d7e40;
            }
            .column {
                float: left;
                width: 50%;
                padding: 10px;
            }
            .poster {
                float: left;
                width: 25%;
                padding: 10px;
            }
            .detail-film {
                float: left;
                width: 75%;
                padding: 10px;
            }
            .row:after {
                content: "";
                display: table;
                clear: both;
            }
            @media screen and (max-width: 600px) {
                .column, .poster, .detail-film {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body class="antialiased">
        <div style="padding:35px;">
            <!-- Tombol untuk ke Landing Page Reservasi Film -->
            <a href="/reservasi-film" class="button previous">&laquo; Reservasi Film</a>
            <!-- Tombol untuk ke Halaman Penonton Film-->
            <a href="/penonton-film" class="button next">Buka Daftar Pelanggan</a>
            <!-- Tombol untuk ke Tugas Lain -->
            <a href="/ganjilgenap" class="button next">Cetak Ganjil Genap</a>
            <a href="/hitungvokal" class="button next">Penghitung Huruf Vokal</a>
            <a href="/provinsi" class="button next">CRUD Data Daerah</a>

            <!-- Menampilkan Error Jika Ada-->
            @if ($errors->any())
                <div style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #1B578A;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #ffffff;">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Menampilkan Pesan Jika Ada-->
            @if (session()->has('message'))
                <div style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #1B578A;">
                    <h2 style="color: #ffffff;">Notifikasi</h2>
                    <p style="color: #ffffff;">{{ session('message') }}</p>
                </div>
            @endif

            <div class="row">
                <!-- Poster Film -->
                <div class="poster">
                    <img src="{{$film->tautan_poster}}" alt="{{$film->nama}}" width="290" height="426"/>
                </div>

                <!-- Detail Film -->
                <div class="detail-film">
                    <h1 style="color:#1B578A;">{{$film->nama}}</h1>
                    @if ($film->rating_usia == "su")
                        <p>Rating Usia: Semua Usia (su)</p>
                    @elseif ($film->rating_usia == "r13")
                        <p>Rating Usia: Remaja (r13+)</p>
                    @else
                        <p>Rating Usia: Dewasa (d17+)</p>
                    @endif
                    <p>Penonton:</p>
                    <ul>
                        @foreach ($penonton as $p)
                            <li>{{$p->nama}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <!-- Div Penampung Dua Form -->
            <div class="row">
                <!-- Form Create Reservasi -->
                <div class="column">
                    <h2 style="color:#1B578A;">Reservasi</h2>
                    <p>Gunakan form ini untuk melakukan reservasi.</p>
                    <form method="post" action="{{route('create-reservasi')}}">
                        @csrf
                        <label for="penonton_id">Atas Nama<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="penonton_id" required>
                                @foreach ($bukanPenonton as $bp)
                                    <option value="{{$bp->id}}">{{$bp->nama}}</option>
                                @endforeach
                            </select>
                        </border><br>
                        <button class="button next submit" name="film_id" value="{{$film->id}}" type="submit">Reservasi</button>
                    </form>
                    <p>*) wajib diisi</p>
                </div>

                <!-- Form Delete Reservasi -->
                <div class="column">
                    <h2 style="color:#1B578A;">Cabut Reservasi</h2>
                    <p>Gunakan form ini untuk mencabut reservasi.</p>
                    <form method="post" action="{{route('delete-reservasi')}}">
                        @csrf
                        <label for="penonton_id">Atas Nama<sup>*</sup>:</label><br>
                        <border style="border: 1px solid #0d7e40;">
                            <select name="penonton_id" required>
                                @foreach ($penonton as $p)
                                    <option value="{{$p->id}}">{{$p->nama}}</option>
                                @endforeach
                            </select>
                        </border><br>
                        <button class="button next submit" name="film_id" value="{{$film->id}}" type="submit">Cabut</button>
                    </form>
                    <p>*) wajib diisi</p>
                </div>
            </div>
        </div>
    </body>
</html>
