<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->string('nama')
                ->nullable(false)
                ->unique();
            $table->string('tautan_poster')
                ->nullable(false)
                ->unique();
            $table->string('rating_usia', 4)
                ->nullable(false);
            $table->timestamps();
        });
        // Constraint nilai dari rating usia
        DB::statement("ALTER TABLE films ADD CONSTRAINT chk_rating_usia CHECK (rating_usia IN ('su', 'r13', 'd17'));");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
