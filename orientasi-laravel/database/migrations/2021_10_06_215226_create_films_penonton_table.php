<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsPenontonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films_penonton', function (Blueprint $table) {
            $table->id();
            $table->foreignId('film_id')
                ->nullable(false)
                ->references('id')->on('films')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('penonton_id')
                ->nullable(false)
                ->references('id')->on('penonton')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
            $table->unique(['film_id', 'penonton_id'], 'film_penonton_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films_penonton');
    }
}
