<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_complaints', function (Blueprint $table) {
            $table->id();
            $table->date('Date Received')->nullable();
            $table->string('Product Name')->nullable();
            $table->string('Sub Product')->nullable();
            $table->string('Issue')->nullable();
            $table->string('Sub Issue')->nullable();
            $table->text('Consumer Complaint Narrative')->nullable();
            $table->string('Company Public Response')->nullable();
            $table->string('Company')->nullable();
            $table->string('State Name', 3)->nullable();
            $table->string('Zip Code')->nullable();
            $table->string('Tags', 31)->nullable();
            $table->string('Consumer Consent Provided', 22)->nullable();
            $table->string('Submitted via', 13)->nullable();
            $table->date('Date Sent to Company')->nullable();
            $table->string('Company Response to Consumer', 33)->nullable();
            $table->string('Timely Response', 4)->nullable();
            $table->string('Consumer Disputed', 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_complaints');
    }
}
