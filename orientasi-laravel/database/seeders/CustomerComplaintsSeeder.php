<?php

namespace Database\Seeders;

use App\Imports\CustomerComplaintsImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class CustomerComplaintsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new CustomerComplaintsImport, 'ConsumerComplaints.csv');
    }
}
