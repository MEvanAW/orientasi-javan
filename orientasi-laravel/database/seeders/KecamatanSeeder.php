<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kecamatan')->insert([
            /*01*/ ['nama' => 'Johan Pahlawan', 'luas' => 45, 'penduduk' => 63975, 'kota_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*02*/ ['nama' => 'Samatiga', 'luas' => 141, 'penduduk' => 15172, 'kota_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*03*/ ['nama' => 'Bubon', 'luas' => 130, 'penduduk' => 7339, 'kota_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*04*/ ['nama' => 'Blang Pidie', 'luas' => 474, 'penduduk' => 22850, 'kota_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*05*/ ['nama' => 'Tangan-Tangan', 'luas' => 133, 'penduduk' => 12339, 'kota_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*06*/ ['nama' => 'Manggeng', 'luas' => 41, 'penduduk' => 13864, 'kota_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*07*/ ['nama' => 'Baitussalam', 'luas' => 21, 'penduduk' => 18878, 'kota_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*08*/ ['nama' => 'Blang Bintang', 'luas' => 42, 'penduduk' => 31983, 'kota_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*09*/ ['nama' => 'Darul Imarah', 'luas' => 24, 'penduduk' => 53177, 'kota_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*10*/ ['nama' => 'Abiansemal', 'luas' => 69, 'penduduk' => 89378, 'kota_id' => 4, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*11*/ ['nama' => 'Kuta', 'luas' => 18, 'penduduk' => 54032, 'kota_id' => 4, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*12*/ ['nama' => 'Kuta Selatan', 'luas' => 101, 'penduduk' => 105434, 'kota_id' => 4, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*13*/ ['nama' => 'Bangli', 'luas' => 56, 'penduduk' => 50480, 'kota_id' => 5, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*14*/ ['nama' => 'Kintamani', 'luas' => 367, 'penduduk' => 93240, 'kota_id' => 5, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*15*/ ['nama' => 'Susut', 'luas' => 49, 'penduduk' => 43202, 'kota_id' => 5, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*16*/ ['nama' => 'Banjar', 'luas' => 173, 'penduduk' => 71890, 'kota_id' => 6, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*17*/ ['nama' => 'Buleleng', 'luas' => 173, 'penduduk' => 135840, 'kota_id' => 6, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*18*/ ['nama' => 'Belinyu', 'luas' => 546, 'penduduk' => 38681, 'kota_id' => 7, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*19*/ ['nama' => 'Sungai Liat', 'luas' => 148, 'penduduk' => 94044, 'kota_id' => 7, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*20*/ ['nama' => 'Muntok', 'luas' => 414, 'penduduk' => 53381, 'kota_id' => 8, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*21*/ ['nama' => 'Bukit Intan', 'luas' => 35, 'penduduk' => 41343, 'kota_id' => 9, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*22*/ ['nama' => 'Gabek', 'luas' => 20, 'penduduk' => 35013, 'kota_id' => 9, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
