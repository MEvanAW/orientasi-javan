<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilmPenontonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films_penonton')->insert([
            /*1*/ ['film_id' => '1', 'penonton_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['film_id' => '1', 'penonton_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
