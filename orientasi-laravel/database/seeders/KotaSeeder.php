<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kota')->insert([
            /*1*/ ['nama' => 'Kabupaten Aceh Barat', 'luas' => 2928, 'penduduk' => 210113, 'provinsi_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Kabupaten Aceh Barat Daya', 'luas' => 1491, 'penduduk' => 150393, 'provinsi_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*3*/ ['nama' => 'Kabupaten Aceh Besar', 'luas' => 2969, 'penduduk' => 425216, 'provinsi_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*4*/ ['nama' => 'Kabupaten Badung', 'luas' => 419, 'penduduk' => 468346, 'provinsi_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*5*/ ['nama' => 'Kabupaten Bangli', 'luas' => 491, 'penduduk' => 264945, 'provinsi_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*6*/ ['nama' => 'Kabupaten Buleleng', 'luas' => 1365, 'penduduk' => 814356, 'provinsi_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*7*/ ['nama' => 'Kabupaten Bangka', 'luas' => 2951, 'penduduk' => 312460, 'provinsi_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*8*/ ['nama' => 'Kabupaten Bangka Barat', 'luas' => 2821, 'penduduk' => 189621, 'provinsi_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*9*/ ['nama' => 'Kota Pangkalpinang', 'luas' => 104, 'penduduk' => 218569, 'provinsi_id' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
