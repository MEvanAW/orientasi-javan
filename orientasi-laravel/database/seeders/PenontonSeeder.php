<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenontonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penonton')->insert([
            /*1*/ ['nama' => 'Muhammad Evan Anindya Wahyuaji', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Naufal Irfani', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*3*/ ['nama' => 'Moch. Safii', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
