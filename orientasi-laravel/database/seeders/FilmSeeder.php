<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insert([
            /*1*/ ['nama' => 'No Time to Die', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/163188974336814_290x426.jpg', 'rating_usia' => 'r13', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Shang-Chi and the Legend of the Ten Rings', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/16303163626913_290x426.jpg', 'rating_usia' => 'su', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*3*/ ['nama' => 'Malignant', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/163118237047331_290x426.jpg', 'rating_usia' => 'd17', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*4*/ ['nama' => 'Jungle Cruise', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/163031668566130_290x426.jpg', 'rating_usia' => 'r13', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*5*/ ['nama' => 'Run Hide Fight', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/163342791898782_290x426.jpg', 'rating_usia' => 'd17', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*6*/ ['nama' => 'Zerre Pendekar Ufuk Timur', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/16333445672951_290x426.jpg', 'rating_usia' => 'r13', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*7*/ ['nama' => 'Dune', 'tautan_poster' => 'https://media.21cineplex.com/webcontent/gallery/pictures/163307215533128_290x426.jpg', 'rating_usia' => 'su', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
