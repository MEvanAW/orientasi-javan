<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinsi')->insert([
            /*1*/ ['nama' => 'Aceh', 'luas' => 57956, 'penduduk' => 5371532, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Bali', 'luas' => 5780, 'penduduk' => 4104900, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*3*/ ['nama' => 'Bangka Belitung', 'luas' => 16424, 'penduduk' => 1343900, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
