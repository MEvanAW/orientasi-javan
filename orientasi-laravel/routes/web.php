<?php

use App\Http\Controllers\CustomerComplaintsController;
use App\Http\Controllers\DaerahController;
use App\Http\Controllers\GanjilGenapController;
use App\Http\Controllers\HasilController;
use App\Http\Controllers\HitungVokalController;
use App\Http\Controllers\KalkulatorController;
use App\Http\Controllers\ReservasiFilmController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// GET Routes
Route::get('/', function () {
    return view('welcome');
});
Route::get('/kalkulator', [KalkulatorController::class, 'kalkulator']);
Route::get('/hasil', [HasilController::class, 'hasil']);
Route::get('/ganjilgenap', [GanjilGenapController::class, 'ganjilgenap']);
Route::get('/hitungvokal', [HitungVokalController::class, 'hitungvokal']);
Route::get('/provinsi', [DaerahController::class, 'provinsi']);
Route::get('/kota', [DaerahController::class, 'kota']);
Route::get('/kecamatan', [DaerahController::class, 'kecamatan']);
Route::get('/reservasi-film', [ReservasiFilmController::class, 'landing']);
Route::get('/penonton-film', [ReservasiFilmController::class, 'penonton']);
Route::get('/film', [ReservasiFilmController::class, 'film']);
Route::get('/company-response', [CustomerComplaintsController::class, 'companyResponse']);

// POST Routes
Route::post('/crud-kecamatan', [DaerahController::class, 'crudKecamatan'])->name('crud-kecamatan');
Route::post('/create-update-film', [ReservasiFilmController::class, 'createUpdateFilm'])->name('create-update-film');
Route::post('/delete-film', [ReservasiFilmController::class, 'deleteFilm'])->name('delete-film');
Route::post('/create-penonton', [ReservasiFilmController::class, 'createPenonton'])->name('create-penonton');
Route::post('/update-penonton', [ReservasiFilmController::class, 'updatePenonton'])->name('update-penonton');
Route::post('/delete-penonton', [ReservasiFilmController::class, 'deletePenonton'])->name('delete-penonton');
Route::post('/create-reservasi', [ReservasiFilmController::class, 'createReservasi'])->name('create-reservasi');
Route::post('/delete-reservasi', [ReservasiFilmController::class, 'deleteReservasi'])->name('delete-reservasi');
