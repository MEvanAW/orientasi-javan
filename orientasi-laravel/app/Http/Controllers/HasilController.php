<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HasilController extends Controller
{
    public function hasil(Request $request){
        $str = "<p class=\"text-gray-600 dark:text-gray-400\">Parameter tidak sesuai.</p>";
        if (is_numeric($request->operand1) && $request->operator != null && is_numeric($request->operand2)) {
            $str = $this->kalkulatorSederhana($request->operand1 . strip_tags($request->operator) . $request-> operand2);
        } elseif ($request->str != null) {
            $str = $this->kalkulatorSederhana(strip_tags($request->str));
        } elseif(is_numeric($request->a) && is_numeric($request->b)) {
            $str = $this->cetakGanjilGenap((int) $request->a, (int) $request->b);
        } elseif($request->strVokal != null) {
            $str = $this->HitungVokal(strip_tags($request->strVokal));
        }
        return view('hasil', compact('str'));
    }

    // fungsi untuk mencetak hasil operasi matematika dasar (operand harus berupa whole numbers)
    function kalkulatorSederhana($str) {
        // mencetak ulang parameter
        $hasil =  "<p class=\"text-gray-600 dark:text-gray-400\">" . $str;
        // menghapus spasi untuk mempermudah pengolahan
        $str = str_replace(" ", "", $str);
        $operandArr = array();
        $operatorArr = array();
        $strlen = strlen($str);
        for ($i = 0; $i < $strlen; $i++) {
            // memasukkan setiap operand ke operandArr
            if (is_numeric($str[$i])) {
                $startIndex = $i;
                while ($i < $strlen - 1) {
                    if (!is_numeric($str[$i + 1])) {
                        break;
                    }
                    else {
                        $i++;
                    }
                }
                array_push($operandArr, (int) substr($str, $startIndex, $i - $startIndex + 1));
            }
            // memasukkan setiap operator ke operatorArr
            elseif ($str[$i] == "+" || $str[$i] == "-"|| $str[$i] == "x" || $str[$i] == "/") {
                array_push($operatorArr, $str[$i]);
            }
            // batalkan bila ada karakter tidak dikenali
            else {
                $hasil = $hasil . ": terdapat karakter tidak dikenali oleh kalkulator: " . $str[$i];
                return $hasil . "</p>";
            }
        }
        // batalkan bila jumlah operand dan jumlah operator tidak cocok
        if (count($operandArr) - count($operatorArr) != 1) {
            $hasil = $hasil . ": banyak operand dan banyak operator tidak cocok.";
            return $hasil . "</p>";
        }
        // melakukan perkalian dan/atau pembagian terlebih dahulu
        for($i = 0; $i < count($operatorArr); $i++) {
            if($operatorArr[$i] == "x") {
                $operandArr[$i + 1] = $operandArr[$i] * $operandArr[$i + 1];
                $operandArr[$i] = 0;
                if ($i != 0 && $operatorArr[$i - 1] == "-") {
                    $operatorArr[$i] = "-";
                } else {
                    $operatorArr[$i] = "+";
                }
            }
            elseif($operatorArr[$i] == "/") {
                if ($operandArr[$i + 1] == 0) {
                    $hasil = $hasil . ": pembagian dengan nol adalah tak terdefinisi.";
                    return $hasil . "</p>";
                }
                $operandArr[$i + 1] = $operandArr[$i] / $operandArr[$i + 1];
                $operandArr[$i] = 0;
                if ($i != 0 && $operatorArr[$i - 1] == "-") {
                    $operatorArr[$i] = "-";
                } else {
                    $operatorArr[$i] = "+";
                }
            }
        }
        // melakukan penjumlahan dan/atau pengurangan
        for($i = 0; $i < count($operatorArr); $i++) {
            if($operatorArr[$i] == "+") {
                $operandArr[$i + 1] = $operandArr[$i] + $operandArr[$i + 1];
            }
            else {
                $operandArr[$i + 1] = $operandArr[$i] - $operandArr[$i + 1];
            }
        }
        // mencetak hasil
        $hasil = $hasil . " = " . end($operandArr);
        return $hasil . "</p>";
    }

    // fungsi untuk mencetak kalimat apakah masing-masing dari barisan angka ganjil atau genap
    function cetakGanjilGenap($A, $B) {
        $str = "genap";
        $returnStr = "<table><tr><th>Angka</th><th>Jenis</th></tr>";
        if (!$this->isEven($A)){
            $str = "ganjil";
        }
        for ($i = $A; $i <= $B; $i++) {
            $returnStr = $returnStr . "<tr><td>" . $i . "</td><td>" . $str . "</td></tr>";
            if ($str == "genap") {
                $str = "ganjil";
            } else {
                $str = "genap";
            }
        }
        return $returnStr . "</table>";
    }

    // fungsi untuk menentukan apakah sebuah angka genap atau tidak
    function isEven($num) {
        return $num%2 == 0;
    }

    // fungsi untuk mencetak hitungan dan rincian huruf vokal pada sebuah string
    function HitungVokal($str) {
        // deklarasi variabel
        $vocalArr = array();
        $returnStr = "<p class=\"text-gray-600 dark:text-gray-400\">";
        // pengecekan setiap huruf vokal
        if (strpos($str, 'a') !== false) {
            array_push($vocalArr, "a");
        }
        if (strpos($str, 'i') !== false) {
            array_push($vocalArr, "i");
        }
        if (strpos($str, 'u') !== false) {
            array_push($vocalArr, "u");
        }
        if (strpos($str, 'e') !== false) {
            array_push($vocalArr, "e");
        }
        if (strpos($str, 'o') !== false) {
            array_push($vocalArr, "o");
        }
        $count = count($vocalArr);
        $returnStr = $returnStr . '"' . $str . '" = ' . $count;
        // tidak perlu merinci huruf vokal bila tidak ada
        if ($count == 0) {
            return $returnStr . "</p>";
        }
        // perincian huruf vokal
        $returnStr = $returnStr . " yaitu " . $vocalArr[0];
        if ($count > 1) {
            for ($i = 1; $i < $count; $i++){
                $returnStr = $returnStr . " ";
                if ($i == $count - 1) {
                    $returnStr = $returnStr . "dan ";
                }
                $returnStr = $returnStr . $vocalArr[$i];
            }
        }
        return $returnStr . "</p>";
    }
}
