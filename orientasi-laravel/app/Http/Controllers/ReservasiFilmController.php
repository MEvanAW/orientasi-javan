<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\FilmPenonton;
use App\Models\Penonton;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class ReservasiFilmController extends Controller
{
    // mengembalikan view landing page reservasi film
    public function landing() {
        $films = Film::all();
        return view('landingreservasi', compact('films'));
    }

    // mengembalikan view halaman penonton film
    public function penonton() {
        $penonton = Penonton::all();
        return view('penontonfilm', compact('penonton'));
    }

    // mengembalikan view halaman detail film
    public function film(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'judul' => 'required|max:255|exists:films,nama'
        ];
        $this->validate($request, $rules);
        $film = Film::where('nama', strip_tags($request->judul))->first();
        if ($film == null) {
            session()->flash('message', 'Film dengan judul "' . $request->judul . '" tidak tersedia.');
            return $this->landing();
        }
        $filmPenonton = FilmPenonton::where('film_id', $film->id)->get();
        $penontonIdArray = array();
        foreach($filmPenonton as $fp) {
            array_push($penontonIdArray, $fp->penonton_id);
        }
        $penonton = DB::table('penonton')->whereIn('id', $penontonIdArray)->get();
        $bukanPenonton = DB::table('penonton')->whereNotIn('id', $penontonIdArray)->get();
        return view('film', compact('film', 'penonton', 'bukanPenonton'));
    }

    // fungsi untuk menangani post create dan update film
    public function createUpdateFilm(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'id' => 'nullable|numeric|exists:films,id',
            'nama' => 'nullable|max:255',
            'tautan_poster' => 'nullable|max:255',
            'rating_usia' => 'required|max:4'
        ];
        $this->validate($request, $rules);
        if ($request->statement == "create" && $request->nama != null && $request->tautan_poster != null) {
            $film = new Film;
            $film->id = DB::table('films')->find(DB::table('films')->max('id'))->id + 1;
            $film->nama = strip_tags($request->nama);
            $film->tautan_poster = $request->tautan_poster;
            $film->rating_usia = $request->rating_usia;
            $film->save();
            return redirect('/reservasi-film')->with('message', 'Film ' . $request->nama . ' berhasil ditambahkan.');
        } elseif($request->statement == "update" && $request->id != null) {
            $film = Film::find($request->id);
            if ($request->nama != null) {
                $film->nama = strip_tags($request->nama);
            }
            if ($request->tautan_poster != null) {
                $film->tautan_poster = strip_tags($request->tautan_poster);
            }
            if ($request->rating_usia == "su" || $request->rating_usia == "r13" || $request->rating_usia == "d17") {
                $film->rating_usia = $request->rating_usia;
            }
            $film->save();
            return redirect('/reservasi-film')->with('message', 'Film ' . $film->nama . ' berhasil di-update.');
        } else {
            return redirect('/reservasi-film')->with('message', 'Post tidak melakukan apa pun. Ini dapat disebabkan karena request yang tidak sesuai.');
        }
    }

    // fungsi untuk menangani post delete film
    public function deleteFilm(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'id' => 'required|numeric|exists:films,id',
        ];
        $this->validate($request, $rules);
        $film = Film::find($request->id);
        $judul = $film->nama;
        $film->delete();
        return redirect('/reservasi-film')->with('message', 'Film ' . $judul . ' berhasil dihapus.');
    }

    // fungsi untuk menangani post create penonton
    public function createPenonton(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'nama' => 'required|max:255'
        ];
        $this->validate($request, $rules);
        $penonton = new Penonton;
        $penonton->id = DB::table('films')->find(DB::table('films')->max('id'))->id + 1;
        $penonton->nama = strip_tags($request->nama);
        $penonton->save();
        return redirect('/penonton-film')->with('message', 'Penonton ' . $request->nama . ' berhasil ditambahkan.');
    }

    // fungsi untuk menangani post update penonton
    public function updatePenonton(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'id' => 'required|numeric|exists:penonton',
            'nama' => 'required|max:255'
        ];
        $this->validate($request, $rules);
        try {
            $penonton = Penonton::find($request->id);
            $penonton->nama = strip_tags($request->nama);
            $penonton->save();
            return redirect('/penonton-film')->with('message', 'Penonton ' . $penonton->nama . ' berhasil di-update.');
        } catch(Throwable $e) {
            return redirect('/penonton-film')->with('message', 'Mohon maaf, pembaruan gagal. Pesan error: ' . $e->getMessage());
        }
    }

    // fungsi untuk menangani post delete penonton
    public function deletePenonton(Request $request) {
        $rules = [
            'id' => 'required|numeric|exists:penonton'
        ];
        $this->validate($request, $rules);
        try {
            $penonton = Penonton::find($request->id);
            $nama = $penonton->nama;
            $penonton->delete();
            return redirect('/penonton-film')->with('message', 'Penonton ' . $nama . ' berhasil dihapus.');
        } catch(Throwable $e) {
            return redirect('/penonton-film')->with('message', 'Mohon maaf, penghapusan gagal. Pesan error: ' . $e->getMessage());
        }
    }

    // fungsi untuk menangani post create reservasi
    public function createReservasi(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'penonton_id' => 'required|numeric|exists:penonton,id',
            'film_id' => 'required|numeric|exists:films,id'
        ];
        $this->validate($request, $rules);
        $filmPenonton = new FilmPenonton;
        $filmPenonton->id = DB::table('films_penonton')->find(DB::table('films_penonton')->max('id'))->id + 1;
        $filmPenonton->film_id = $request->film_id;
        $filmPenonton->penonton_id = $request->penonton_id;
        $filmPenonton->save();
        $namaFilm = Film::find($request->film_id)->nama;
        $namaPemesan = Penonton::find($request->penonton_id)->nama;
        return redirect('/film?judul=' . $namaFilm)->with('message', 'Reservasi atas nama ' . $namaPemesan . ' berhasil ditambahkan.');
    }
    // fungsi untuk menangani post delete reservasi
    public function deleteReservasi(Request $request)
    {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'penonton_id' => 'required|numeric|exists:penonton,id',
            'film_id' => 'required|numeric|exists:films,id'
        ];
        $this->validate($request, $rules);
        try {
            $namaFilm = Film::find($request->film_id)->nama;
            $namaPemesan = Penonton::find($request->penonton_id)->nama;
            $reservasi = FilmPenonton::where([
                ['film_id', '=', $request->film_id],
                ['penonton_id', '=', $request->penonton_id]
            ])->first();
            $reservasi->delete();
            return redirect('/film?judul=' . $namaFilm)->with('message', 'Reservasi atas nama ' . $namaPemesan . ' berhasil dihapus.');
        } catch(Throwable $e) {
            if (isset($namaFilm)){
                return redirect('/film?judul=' . $namaFilm)->with('message', 'Mohon maaf, pencabutan reservasi gagal. Pesan error: ' . $e->getMessage());
            } else {
                return redirect('/reservasi-film')->with('message', 'Mohon maaf, pencabutan reservasi gagal. Pesan error: ' . $e->getMessage());
            }
        }
    }
}
