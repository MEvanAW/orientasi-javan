<?php

namespace App\Http\Controllers;

use App\Models\CustomerComplaint;
use Illuminate\Http\Request;
use Throwable;

class CustomerComplaintsController extends Controller
{
    public function companyResponse() {
        $table = "<table><tr><th>Company</th><th>Closed with Explanation</th><th>Closed with Monetary Relief</th>" .
            "<th>Closed with Non-Monetary Relief</th><th>Closed</th><th>Untimely Response</th></tr>";
        try {
            $companies = CustomerComplaint::select('Company')->distinct()->get();
        } catch (Throwable $e) {
            session()->flash('message', 'Mohon maaf, terjadi error dengan pesan: ' . $e->getMessage());
        }
        if (isset($companies)){
            $companies = $companies->slice(0, 25)->sort();
            $field = 'Company Response to Consumer';
            foreach ($companies as $company) {
                $table = $table . "<tr><td>" . $company->Company . "</td><td>";
                try {
                    $table = $table . CustomerComplaint::where([$field => 'Closed with explanation',
                            'Company' => $company->Company])->count() . "</td><td>" .
                        CustomerComplaint::where([$field => 'Closed with monetary relief',
                            'Company' => $company->Company])->count() . "</td><td>" .
                        CustomerComplaint::where([$field => 'Closed with non-monetary relief',
                            'Company' => $company->Company])->count() . "</td><td>" .
                        CustomerComplaint::where([$field => 'Closed',
                            'Company' => $company->Company])->count() . "</td><td>" .
                        CustomerComplaint::where([$field => 'Untimely response',
                            'Company' => $company->Company])->count() . "</td></tr>";
                } catch (Throwable $e) {
                    session()->flash('message', 'Mohon maaf, terjadi error dengan pesan: ' . $e->getMessage());
                }
            }
        }
        $table = $table . "</table>";
        return view('companyresponse', compact('table'));
    }
}
