<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;

class DaerahController extends Controller
{
    // mengembalikan view halaman provinsi dan menangani CRUD provinsi
    public function provinsi(Request $request) {
        if ($request->statement == "create" && $request->nama != null && is_numeric($request->luas) && is_numeric($request->penduduk)) {
            $provinsi = new Provinsi;
            $provinsi->id = DB::table('provinsi')->find(DB::table('provinsi')->max('id'))->id + 1;
            $provinsi->nama = strip_tags($request->nama);
            $provinsi->luas = $request->luas;
            $provinsi->penduduk = $request->penduduk;
            $provinsi->save();
            session()->flash('message', 'Provinsi ' . $request->nama . ' berhasil ditambahkan.');
        } elseif ($request->statement == "update" && is_numeric($request->id)) {
            $provinsi = Provinsi::find($request->id);
            if ($request->nama != null) {
                $provinsi->nama = strip_tags($request->nama);
            }
            if (is_numeric($request->luas)) {
                $provinsi->luas = $request->luas;
            }
            if (is_numeric($request->penduduk)) {
                $provinsi->penduduk = $request->penduduk;
            }
            $provinsi->save();
            session()->flash('message', 'Provinsi ' . $provinsi->nama . ' berhasil di-update.');
        } elseif ($request->statement == "delete" && is_numeric($request->id)) {
            $provinsi = Provinsi::find($request->id);
            $provinsi->delete();
            session()->flash('message', 'Provinsi dengan ID=' . $request->id . ' berhasil dihapus.');
        }
        // tabel provinsi untuk dikirim ke blade melalui compact
        $table = "<table><tr><th>ID</th><th>Provinsi</th><th>Luas (km<sup>2</sup>)</th><th>Jumlah Penduduk (jiwa)</th></tr>";
        foreach (Provinsi::all() as $provinsi) {
            $table = $table . "<tr><td>" . $provinsi->id . "</td><td>" .
                "<a href='/kota?provinsi=" . $provinsi->id . "'>" . $provinsi->nama . "</a></td><td>" .
                $provinsi->luas . "</td><td>" .
                $provinsi->penduduk . "</td></tr>";
        }
        $table = $table . "</table>";
        return view('provinsi', compact('table'));
    }

    // mengembalikan view halaman kota dan menangani CRUD kota
    public function kota(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid (bila ada)
        $rules = [
            'provinsi' => ["nullable", "exists:provinsi,id"],
            'id' => 'nullable|numeric|exists:kota',
            'nama' => 'nullable|max:255',
            'luas' => 'nullable|numeric|max:120000',
            'penduduk' => 'nullable|numeric|max:10000000',
            'provinsi_id' => 'nullable|numeric|exists:provinsi,id',
            'statement' => 'nullable|max:8'
        ];
        $this->validate($request, $rules);
        if ($request->statement == "create" && $request->nama != null && $request->luas != null && $request->penduduk != null && $request->provinsi_id != null) {
            $kota = new Kota;
            $kota->id = DB::table('kota')->find(DB::table('kota')->max('id'))->id + 1;
            $kota->nama = strip_tags($request->nama);
            $kota->luas = $request->luas;
            $kota->penduduk = $request->penduduk;
            $kota->provinsi_id = $request->provinsi_id;
            $kota->save();
            session()->flash('message', 'Kota ' . $request->nama . ' berhasil ditambahkan.');
        } elseif ($request->statement == "update" && $request->id != null) {
            $kota = Kota::find($request->id);
            if ($request->nama != null) {
                $kota->nama = strip_tags($request->nama);
            }
            if ($request->luas != null) {
                $kota->luas = $request->luas;
            }
            if ($request->penduduk != null) {
                $kota->penduduk = $request->penduduk;
            }
            if ($request->provinsi_id != null) {
                $kota->provinsi_id = $request->provinsi_id;
            }
            $kota->save();
            session()->flash('message', 'Kota ' . $kota->nama . ' berhasil di-update.');
        } elseif ($request->statement == "delete" && $request->id != null) {
            $kota = Kota::find($request->id);
            $kota->delete();
            session()->flash('message', 'Kota dengan ID=' . $request->id . ' berhasil dihapus.');
        }
        // empat variabel untuk dikirim ke blade melalui compact
        $h1 = "<h1 style=\"color:#EF3B2D;\">CRUD Data Kota/Kabupaten";
        $table = "<table><tr><th>ID</th><th>Kota/Kabupaten</th><th>Provinsi</th><th>Luas (km<sup>2</sup>)</th><th>Jumlah Penduduk (jiwa)</th></tr>";
        $namaProvinsi = "";
        $jsonProvinces = "";
        // pengolahan ketiga variabel bila ada request id provinsi
        if(is_numeric($request->provinsi)) {
            $namaProvinsi = Provinsi::find($request->provinsi)->nama;
            $jsonProvinces = "{\"" . $request->provinsi . "\":\"" . $namaProvinsi . "\"}";
            $h1 = $h1 . " di Provinsi " . $namaProvinsi;
            foreach (Kota::where('provinsi_id', $request->provinsi)->get() as $kota) {
                $table = $table . "<tr><td>" . $kota->id . "</td><td>" .
                    "<a href='/kecamatan?kota=" . $kota->id . "'>" . $kota->nama . "</a></td><td>" .
                    $namaProvinsi . "</td><td>" .
                    $kota->luas . "</td><td>" .
                    $kota->penduduk . "</td></tr>";
            }
        } else { // pengolahan tabel bila tidak ada request
            $provinces = Provinsi::select('id', 'nama')->get();
            $jsonProvinces = $this->daerahToJsonString($provinces);
            foreach (Kota::all() as $kota) {
                $table = $table . "<tr><td>" . $kota->id . "</td><td>" .
                    "<a href='/kecamatan?kota=" . $kota->id . "'>" . $kota->nama . "</a></td><td>" .
                    Provinsi::find($kota->provinsi_id)->nama . "</td><td>" .
                    $kota->luas . "</td><td>" .
                    $kota->penduduk . "</td></tr>";
            }
        }
        $h1 = $h1 . "</h1>";
        $table = $table . "</table>";
        return view('kota', compact('table', 'h1', 'namaProvinsi', 'jsonProvinces'));
    }

    // mengembalikan view halaman kecamatan
    public function kecamatan(Request $request) {
        // empat variabel untuk dikirim ke blade melalui compact
        $h1 = "<h1 style=\"color:#EF3B2D;\">CRUD Data Kecamatan";
        $table = "<table><tr><th>ID</th><th>Kecamatan</th><th>Kota/Kabupaten</th><th>Provinsi</th><th>Luas (km<sup>2</sup>)</th><th>Jumlah Penduduk (jiwa)</th></tr>";
        $namaKota = "";
        // pengolahan ketiga variabel bila ada request id kota
        if (is_numeric($request->kota)) {
            $h1 = $h1 . " di " . $namaKota;
            $cities = Kota::select('id', 'nama')->where('kota_id', $request->kota)->get();
            $namaKota = $cities[0]->nama;
            foreach (Kecamatan::where('kota_id', $request->kota)->get() as $kecamatan) {
                $kota = Kota::find($kecamatan->kota_id);
                $table = $table . "<tr><td>" . $kecamatan->id . "</td><td>" .
                    $kecamatan->nama . "</td><td>" .
                    $kota->nama . "</td><td>" .
                    Provinsi::find($kota->provinsi_id)->nama . "</td><td>" .
                    $kecamatan->luas . "</td><td>" .
                    $kecamatan->penduduk . "</td></tr>";
            }
        } else { // pengolahan tabel bila tidak ada request
            $cities = Kota::select('id', 'nama')->get();
            foreach (Kecamatan::all() as $kecamatan) {
                $kota = Kota::find($kecamatan->kota_id);
                $table = $table . "<tr><td>" . $kecamatan->id . "</td><td>" .
                    $kecamatan->nama . "</td><td>" .
                    $kota->nama . "</td><td>" .
                    Provinsi::find($kota->provinsi_id)->nama . "</td><td>" .
                    $kecamatan->luas . "</td><td>" .
                    $kecamatan->penduduk . "</td></tr>";
            }
        }
        $h1 = $h1 . "</h1>";
        $table = $table . "</table>";
        return view('kecamatan', compact('table', 'h1', 'namaKota', 'cities'));
    }

    // menangani create kecamatan
    function crudKecamatan(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid (bila ada)
        $rules = [
            'id' => 'nullable|numeric|exists:kecamatan',
            'nama' => 'nullable|max:255',
            'luas' => 'nullable|numeric|max:120000',
            'penduduk' => 'nullable|numeric|max:10000000',
            'kota_id' => 'nullable|numeric|exists:kota,id',
            'statement' => 'required|max:8'
        ];
        $this->validate($request, $rules);
        if ($request->statement == "create" && $request->nama != null && $request->luas != null && $request->penduduk != null && $request->kota_id != null) {
            $kecamatan = new Kecamatan;
            $kecamatan->id = DB::table('kecamatan')->find(DB::table('kecamatan')->max('id'))->id + 1;
            $kecamatan->nama = strip_tags($request->nama);
            $kecamatan->luas = $request->luas;
            $kecamatan->penduduk = $request->penduduk;
            $kecamatan->kota_id = $request->kota_id;
            $kecamatan->save();
            return redirect('/kecamatan')->with('message', 'Kecamatan ' . $request->nama . ' berhasil ditambahkan.');
        } elseif ($request->statement == "update" && $request->id != null && $request->kota_id != null) {
            $kecamatan = Kecamatan::find($request->id);
            if ($request->nama != null) {
                $kecamatan->nama = strip_tags($request->nama);
            }
            if ($request->luas != null) {
                $kecamatan->luas = $request->luas;
            }
            if ($request->penduduk != null) {
                $kecamatan->penduduk = $request->penduduk;
            }
            $kecamatan->kota_id = $request->kota_id;
            $kecamatan->save();
            return redirect('/kecamatan')->with('message', 'Kota ' . $kecamatan->nama . ' berhasil di-update.');
        } elseif($request->statement == "delete" && $request->id != null) {
            $kecamatan = Kecamatan::find($request->id);
            $kecamatan->delete();
            return redirect('/kecamatan')->with('message', 'Kecamatan dengan ID=' . $request->id . ' berhasil dihapus.');
        } else{
            return redirect('/kecamatan')->with('message', 'Post tidak melakukan apa pun. Ini dapat disebabkan karena request yang tidak sesuai.');
        }
    }

    // mengubah id dan nama daerah ke json
    function daerahToJsonString($daerahRecords) {
        $jsonString = "{";
        foreach ($daerahRecords as $daerahRecord) {
            $jsonString = $jsonString . '"' . $daerahRecord->id . '":"' . $daerahRecord->nama . '",';
        }
        $jsonString = rtrim($jsonString, ",") . "}";
        return $jsonString;
    }
}
