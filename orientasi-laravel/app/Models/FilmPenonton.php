<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FilmPenonton extends Pivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'films_penonton';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'film_id',
        'penonton_id'
    ];
}
