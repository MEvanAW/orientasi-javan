<?php

namespace App\Imports;

use App\Models\CustomerComplaint;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CustomerComplaintsImport implements ToModel, WithBatchInserts, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        CustomerComplaint::create([
            'Date Received' => $row[0],
            'Product Name' => $row[1],
            'Sub Product' => $row[2],
            'Issue' => $row[3],
            'Sub Issue' => $row[4],
            'Consumer Complaint Narrative' => $row[5],
            'Company Public Response' => $row[6],
            'Company' => $row[7],
            'State Name' => $row[8],
            'Zip Code' => $row[9],
            'Tags' => $row[10],
            'Consumer Consent Provided' => $row[11],
            'Submitted via' => $row[12],
            'Date Sent to Company' => $row[13],
            'Company Response to Consumer' => $row[14],
            'Timely Response' => $row[15],
            'Consumer Disputed' => $row[16],
            'id' => $row[17]
        ]);
    }

    public function chunkSize(): int {
        return 500;
    }

    public function batchSize(): int {
        return 500;
    }
}
