# Query CEO
SELECT nama as CEO FROM employee WHERE atasan_id is null;
# Query Staff Biasa
SELECT nama as `Staff biasa` FROM employee WHERE atasan_id > 3;
# Query Direktur
SELECT nama as Direktur FROM employee WHERE atasan_id = 1;
# Query Manager
SELECT nama as Manager FROM employee WHERE atasan_id > 1 AND atasan_id <= 3;
# Query Jumlah Bawahan. Ganti 'Pak Budi' dengan nama yang ingin dicari jumlah bawahannya.
WITH RECURSIVE cte (id, nama, atasan_id) as (
    SELECT id, nama, atasan_id FROM employee WHERE nama = 'Pak Budi'
    UNION ALL
    SELECT e.id, e.nama, e.atasan_id FROM employee as e
    INNER JOIN cte ON e.atasan_id = cte.id
) SELECT nama, COUNT(*) - 1 as `jumlah bawahan` FROM cte;