CREATE TABLE employee (
    id int NOT NULL,
    nama varchar(251) NOT NULL,
    atasan_id int,
    company_id int NOT NULL,
    CONSTRAINT PK_id PRIMARY KEY (id),
    CONSTRAINT FK_company_id_id FOREIGN KEY (company_id) REFERENCES company(id),
    CONSTRAINT FK_atasan_id_id FOREIGN KEY (atasan_id) REFERENCES employee(id)
);