CREATE TABLE karyawan (
    id int NOT NULL,
    nama varchar(255) NOT NULL,
    `jenis kelamin` varchar(2) NOT NULL,
    status varchar(8) NOT NULL,
    `tanggal lahir` date NOT NULL,
    `tanggal masuk` date NOT NULL,
    departemen int NOT NULL,
    CONSTRAINT PK_id PRIMARY KEY (id),
    CONSTRAINT FK_departemen_id FOREIGN KEY (departemen) REFERENCES departemen(id),
    CONSTRAINT `CHECK_jenis kelamin` CHECK (`jenis kelamin` IN ('L', 'P')),
    CONSTRAINT CHECK_status CHECK (status IN ('Menikah', 'Belum'))
);