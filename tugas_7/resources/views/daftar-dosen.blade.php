@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="/" class="nav previous">&laquo; Beranda</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Daftar Dosen</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Tabel Dosen -->
        <table style="margin-top:15px;">
            <caption>Tabel Dosen</caption>
            <tr>
                <th id="ID">ID</th>
                <th id="nama">Nama</th>
                <th id="NIP">NIP</th>
                <th id="gelar">Gelar</th>
                <th id="riwayat pendidikan">Riwayat Pendidikan</th>
            </tr>
            @foreach ($dosenCollection as $dosen)
                <tr>
                    <td>{{$dosen->id}}</td>
                    <td>{{$dosen->nama}}</td>
                    <td>{{$dosen->nip}}</td>
                    <td>{{$dosen->gelar}}</td>
                    <td><a href="{{route('riwayat-pendidikan', ['dosenId' => $dosen->id])}}">lihat/edit</a></td>
                </tr>
            @endforeach
        </table>

        <!-- Form Tambahkan Dosen -->
        <h2 style="color:#ef3b2d;">Tambahkan Dosen</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan dosen.</p>
        <form method="post" action="{{route('tambahkan-dosen')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="nama">Nama<sup>*</sup>:</label><br>
            <input type="text" id="nama" name="nama" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="nip">NIP<sup>*</sup>:</label><br>
            <input type="text" id="nip" name="nip" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="gelar">Gelar<sup>*</sup>:</label><br>
            <input type="text" id="gelar" name="gelar" required><br>
            <button type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Dosen</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Perbarui Dosen -->
        <h2 style="color:#ef3b2d;">Perbarui Dosen</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui dosen.</p>
        <form method="post" action="{{route('perbarui-dosen')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama*:</label><br>
            <select id="id" name="id" required>
                @foreach($dosenCollection as $dosen)
                    <option value="{{$dosen->id}}">{{$dosen->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="nama">Ubah nama menjadi:</label><br>
            <input type="text" id="nama" name="nama"><br>
            <label class="text-gray-600 dark:text-gray-400" for="nip">NIP:</label><br>
            <input type="text" id="nip" name="nip"><br>
            <label class="text-gray-600 dark:text-gray-400" for="gelar">Gelar:</label><br>
            <input type="text" id="gelar" name="gelar"><br>
            <button type="submit" class="nav next" style="margin-top:10px;">Perbarui Dosen</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Hapus Dosen -->
        <h2 style="color:#ef3b2d;">Hapus Dosen</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus dosen.</p>
        <form method="post" action="{{route('hapus-dosen')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama*:</label><br>
            <select id="id" name="id" required>
                @foreach($dosenCollection as $dosen)
                    <option value="{{$dosen->id}}">{{$dosen->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">- Hapus Dosen</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
    </div>
@stop
