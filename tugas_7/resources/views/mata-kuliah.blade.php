@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="/" class="nav previous">&laquo; Beranda</a>
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Daftar Mata Kuliah</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Tabel Daftar Mata Kuliah -->
        <table>
            <caption>Tabel Daftar Mata Kuliah</caption>
            <tr>
                <th id="ID">ID</th>
                <th id="mata kuliah">Mata Kuliah</th>
                <th id="SKS">SKS</th>
            </tr>
            @foreach($mataKuliahCollection as $mataKuliah)
                <tr>
                    <td>{{$mataKuliah->id}}</td>
                    <td>{{$mataKuliah->nama}}</td>
                    <td>{{$mataKuliah->SKS}}</td>
                </tr>
            @endforeach
        </table>

        <!-- Tombol ke Mata Kuliah Terselenggara-->
        <a href="{{route('matkul-ditawarkan')}}" class="nav next" style="margin-top:10px;">ke Mata Kuliah Ditawarkan</a>

        <!-- Form Tambahkan Mata Kuliah -->
        <h2 style="color:#ef3b2d;">Tambahkan Mata Kuliah</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan mata kuliah.</p>
        <form method="post" action="{{ route('tambahkan-matkul') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="nama">Nama<sup>*</sup>:</label><br>
            <input type="text" id="nama" name="nama" value="{{ old('nama') }}" required><br>
            <label class="text-gray-600 dark:text-gray-400" for="SKS">SKS<sup>*</sup>:</label><br>
            <input type="number" id="SKS" name="SKS" value="{{ old('SKS') }}" required><br>
            <button type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Mata Kuliah</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Perbarui Mata Kuliah -->
        <h2 style="color:#ef3b2d;">Perbarui Mata Kuliah</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui mata kuliah. Isian opsional yang kosong akan dianggap sama dengan sebelumnya.</p>
        <form method="post" action="{{ route('perbarui-matkul') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($mataKuliahCollection as $mataKuliah)
                    <option value="{{ $mataKuliah->id }}" {{ old('id') == $mataKuliah->id ? 'selected' : '' }}>{{$mataKuliah->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="nama">Ubah nama menjadi:</label><br>
            <input type="text" id="nama" name="nama" value="{{ old('nama') }}"><br>
            <label class="text-gray-600 dark:text-gray-400" for="SKS">SKS:</label><br>
            <input type="number" id="SKS" name="SKS" value="{{ old('SKS') }}"><br>
            <button type="submit" class="nav next" style="margin-top:10px;">Perbarui Mata Kuliah</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Hapus Mata Kuliah -->
        <h2 style="color:#ef3b2d;">Hapus Mata Kuliah</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus mata kuliah.</p>
        <form method="post" action="{{ route('hapus-matkul') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($mataKuliahCollection as $mataKuliah)
                    <option value="{{ $mataKuliah->id }}" {{ old('id') == $mataKuliah->id ? 'selected' : '' }}>{{$mataKuliah->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">- Hapus Mata Kuliah</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
    </div>
@stop
