@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="/" class="nav previous">&laquo; Beranda</a>
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Daftar Mahasiswa</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Tabel Daftar Mahasiswa -->
        <table>
            <caption>Tabel Daftar Mahasiswa</caption>
            <tr>
                <th id="ID">ID</th>
                <th id="nama">Nama</th>
                <th id="NIM">NIM</th>
                <th id="jenis kelamin">Jenis Kelamnin</th>
                <th id="tempat lahir">Tempat Lahir</th>
                <th id="tanggal lahir">Tanggal Lahir</th>
                <th id="KRS">KRS</th>
            </tr>
            @foreach ($mahasiswaCollection as $mahasiswa)
                <tr>
                    <td>{{$mahasiswa->id}}</td>
                    <td>{{$mahasiswa->nama}}</td>
                    <td>{{$mahasiswa->nim}}</td>
                    <td>{{$mahasiswa->jenis_kelamin}}</td>
                    <td>{{$mahasiswa->tempat_lahir}}</td>
                    <td>{{$mahasiswa->tanggal_lahir}}</td>
                    <td><a href="{{route('krs', ['mahasiswaId' => $mahasiswa->id])}}">isi/lihat</a></td>
                </tr>
            @endforeach
        </table>

        <!-- Form Tambahkan Mahasiswa -->
        <h2 style="color:#ef3b2d;">Tambahkan Mahasiswa</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan mahasiswa.</p>
        <form method="post" action="{{route('tambahkan-mahasiswa')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="nama">Nama<sup>*</sup>:</label><br>
            <input type="text" id="nama" name="nama" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="nim">NIM<sup>*</sup>:</label><br>
            <input type="text" id="nim" name="nim" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="jenis_kelamin">Jenis Kelamin<sup>*</sup>:</label><br>
            <select id="jenis_kelamin" name="jenis_kelamin" required>
                <option value="laki-laki">Laki-laki</option>
                <option value="perempuan">Perempuan</option>
            </select><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tempat_lahir">Tempat Lahir<sup>*</sup>:</label><br>
            <input type="text" id="tempat_lahir" name="tempat_lahir" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tanggal_lahir">Tanggal Lahir<sup>*</sup>:</label><br>
            <input type="date" id="tanggal_lahir" name="tanggal_lahir" required><br>
            <button type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Mahasiswa</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Perbarui Mahasiswa -->
        <h2 style="color:#ef3b2d;">Perbarui Data Mahasiswa</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui data mahasiswa. Field opsional yang dikosongkan dianggap sama dengan data sebelumnya.</p>
        <form method="post" action="{{route('perbarui-mahasiswa')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($mahasiswaCollection as $mahasiswa)
                    <option value="{{$mahasiswa->id}}">{{$mahasiswa->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="nama">Ubah nama menjadi:</label><br>
            <input type="text" id="nama" name="nama"><br>
            <label class= "text-gray-600 dark:text-gray-400" for="nim">NIM:</label><br>
            <input type="text" id="nim" name="nim"><br>
            <label class= "text-gray-600 dark:text-gray-400" for="jenis_kelamin">Jenis Kelamin:</label><br>
            <select id="jenis_kelamin" name="jenis_kelamin">
                <option value="">Edit jenis kelamin...</option>
                <option value="laki-laki">Laki-laki</option>
                <option value="perempuan">Perempuan</option>
            </select><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tempat_lahir">Tempat Lahir:</label><br>
            <input type="text" id="tempat_lahir" name="tempat_lahir"><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tanggal_lahir">Tanggal Lahir:</label><br>
            <input type="date" id="tanggal_lahir" name="tanggal_lahir"><br>
            <button type="submit" class="nav next" style="margin-top:10px;">Perbarui Data Mahasiswa</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Hapus Data Mahasiswa -->
        <h2 style="color:#ef3b2d;">Hapus Data Mahasiswa</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus data mahasiswa.</p>
        <form method="post" action="{{route('hapus-mahasiswa')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($mahasiswaCollection as $mahasiswa)
                    <option value="{{$mahasiswa->id}}">{{$mahasiswa->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">- Hapus Data Mahasiswa</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
    </div>
@stop
