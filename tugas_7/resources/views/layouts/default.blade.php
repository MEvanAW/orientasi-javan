<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body>
    <div class="container">

        <!-- Header -->
        <header class="row">
            @include('includes.header')
        </header>

        <!-- Main Content -->
        <div id="main" class="row antialiased">
            @yield('content')
        </div>

        </div>
    </body>
</html>
