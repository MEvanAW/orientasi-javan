@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="{{route('daftar-mahasiswa')}}" class="nav previous">&laquo; Daftar Mahasiswa</a>
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Kartu Rencana Studi (KRS)</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Paragraf Informasi Mahasiswa -->
        <p class="text-gray-600 dark:text-gray-400">Nama: {{$mahasiswa->nama}}</p>
        <p class="text-gray-600 dark:text-gray-400">NIM: {{$mahasiswa->nim}}</p>

        <!-- Tabel Mata Kuliah Ditawarkan -->
        <table>
            <caption>Tabel Mata Kuliah Ditawarkan</caption>
            <tr>
                <th id="ID">No</th>
                <th id="mata kuliah">Mata Kuliah</th>
                <th id="SKS">SKS</th>
                <th id="SKS">Dosen</th>
                <th id="SKS">Ruang</th>
                <th id="aksi">Aksi</th>
            </tr>
            @foreach($matkulDitawarkanCollection as $matkulDitawarkan)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$matkulDitawarkan->mataKuliah->nama}}</td>
                    <td>{{$matkulDitawarkan->mataKuliah->SKS}}</td>
                    <td>{{$matkulDitawarkan->dosen->nama}}</td>
                    <td>{{$matkulDitawarkan->ruangKelas->nama}}</td>
                    <td>
                        @if ($krs->search(function ($butirKrs, $key) use($matkulDitawarkan) {
                           return $butirKrs->mata_kuliah_ditawarkan_id == $matkulDitawarkan->id;
                        }) !== false)
                            sudah diambil
                        @else
                            <form method="post" action="{{route('ambil-matkul')}}">
                                @csrf
                                <input type="hidden" name="mahasiswa_id" value="{{$mahasiswa->id}}">
                                <button type="submit" name="mata_kuliah_ditawarkan_id" value="{{$matkulDitawarkan->id}}">ambil</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        <br>

        <!-- Tabel KRS -->
        @if ($krs->isNotEmpty())
            <table>
                <caption>Tabel KRS</caption>
                <tr>
                    <th id="ID">No</th>
                    <th id="mata kuliah">Mata Kuliah</th>
                    <th id="SKS">SKS</th>
                    <th id="SKS">Dosen</th>
                    <th id="SKS">Ruang</th>
                    <th id="aksi">Aksi</th>
                </tr>
                @foreach ($krs as $butirKrs)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$butirKrs->mataKuliahDitawarkan->mataKuliah->nama}}</td>
                        <td>{{$butirKrs->mataKuliahDitawarkan->mataKuliah->SKS}}</td>
                        <td>{{$butirKrs->mataKuliahDitawarkan->dosen->nama}}</td>
                        <td>{{$butirKrs->mataKuliahDitawarkan->ruangKelas->nama}}</td>
                        <td>
                            <form method="post" action="{{route('batalkan-pengambilan')}}">
                                @csrf
                                <input type="hidden" name="mahasiswa_id" value="{{$mahasiswa->id}}">
                                <button type="submit" name="id" value="{{$butirKrs->id}}">batalkan</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <br>
            <p class="text-gray-600 dark:text-gray-400">KRS masih kosong. Silakan ambil mata kuliah ditawarkan.</p>
        @endif
    </div>
@stop
