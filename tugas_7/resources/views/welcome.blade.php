@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Beranda</h1>

        <!-- Deskripsi Halaman -->
        <p class="text-gray-600 dark:text-gray-400">Selamat datang di Sistem Informasi Kampus. Silakan gunakan tombol di bawah untuk navigasi ke fitur-fitur yang tersedia.</p>
        <p class="text-gray-600 dark:text-gray-400">Riwayat Pendidikan dapat diakses dari Daftar Dosen, sedangkan KRS dapat diakses dari Daftar Mahasiswa.</p>

        <!-- Tombol ke Fitur-Fitur -->
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

    </div>
@stop
