@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="/" class="nav previous">&laquo; Beranda</a>
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Daftar Ruang Kelas</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Tabel Daftar Ruang Kelas -->
        <table>
            <caption>Tabel Daftar Ruang Kelas</caption>
            <tr>
                <th id="ID">ID</th>
                <th id="nama ruang">Nama Ruang</th>
            </tr>
            @foreach ($ruangKelasCollection as $ruangKelas)
                <tr>
                    <td>{{ $ruangKelas->id }}</td>
                    <td>{{ $ruangKelas->nama }}</td>
                </tr>
            @endforeach
        </table>

        <!-- Form Tambahkan Ruang Kelas -->
        <h2 style="color:#ef3b2d;">Tambahkan Ruang Kelas</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan ruang kelas.</p>
        <form method="post" action="{{ route('tambahkan-ruang') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="nama">Nama<sup>*</sup>:</label><br>
            <input type="text" id="nama" name="nama" value="{{ old('nama') }}" required><br>
            <button type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Ruang Kelas</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Perbarui Ruang Kelas -->
        <h2 style="color:#ef3b2d;">Perbarui Ruang Kelas</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui ruang kelas.</p>
        <form method="post" action="{{ route('perbarui-ruang') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($ruangKelasCollection as $ruangKelas)
                    <option value="{{ $ruangKelas->id }}" {{ old('id') == $ruangKelas->id ? 'selected' : '' }}>{{$ruangKelas->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="nama">Ubah nama menjadi<sup>*</sup>:</label><br>
            <input type="text" id="nama" name="nama" value="{{ old('nama') }}" required><br>
            <button type="submit" class="nav next" style="margin-top:10px;">Perbarui Ruang Kelas</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Hapus Ruang Kelas -->
        <h2 style="color:#ef3b2d;">Hapus Ruang Kelas</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus ruang kelas.</p>
        <form method="post" action="{{ route('hapus-ruang') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">Nama<sup>*</sup>:</label><br>
            <select id="id" name="id">
                @foreach($ruangKelasCollection as $ruangKelas)
                    <option value="{{ $ruangKelas->id }}" {{ old('id') == $ruangKelas->id ? 'selected' : '' }}>{{$ruangKelas->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">- Hapus Ruang Kelas</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
    </div>
@stop
