@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="{{route('mata-kuliah')}}" class="nav previous">&laquo; Daftar Mata Kuliah</a>
        <a href="{{route('daftar-dosen')}}" class="nav next">Daftar Dosen</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Mata Kuliah Ditawarkan</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Tabel Daftar Mata Kuliah Ditawarkan -->
        <table>
            <caption>Tabel Mata Kuliah Ditawarkan</caption>
            <tr>
                <th id="ID">ID</th>
                <th id="mata kuliah">Mata Kuliah</th>
                <th id="SKS">SKS</th>
                <th id="SKS">Dosen</th>
                <th id="SKS">Ruang</th>
            </tr>
            @foreach($matkulDitawarkanCollection as $matkulDitawarkan)
                <tr>
                    <td>{{$matkulDitawarkan->id}}</td>
                    <td>{{$matkulDitawarkan->mataKuliah->nama}}</td>
                    <td>{{$matkulDitawarkan->mataKuliah->SKS}}</td>
                    <td>{{$matkulDitawarkan->dosen->nama}}</td>
                    <td>{{$matkulDitawarkan->ruangKelas->nama}}</td>
                </tr>
            @endforeach
        </table>

        <!-- Form Tambahkan Mata Kuliah Ditawarkan -->
        <h2 style="color:#ef3b2d;">Tambahkan Mata Kuliah Ditawarkan</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan mata kuliah ditawarkan.</p>
        <form method="post" action="{{ route('tambahkan-matkul-ditawarkan') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="mata_kuliah_id">Mata Kuliah<sup>*</sup>:</label><br>
            <select id="mata_kuliah_id" name="mata_kuliah_id" required>
                @foreach($namaMatkulCollection as $namaMatkul)
                    <option value="{{ $namaMatkul->id }}" {{ old('mata_kuliah_id') == $namaMatkul->id ? 'selected' : '' }}>{{$namaMatkul->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="dosen_id">Dosen<sup>*</sup>:</label><br>
            <select id="dosen_id" name="dosen_id" required>
                @foreach($namaDosenCollection as $namaDosen)
                    <option value="{{ $namaDosen->id }}" {{ old('dosen_id') == $namaDosen->id ? 'selected' : '' }}>{{$namaDosen->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="ruang_kelas_id">Ruang Kelas<sup>*</sup>:</label><br>
            <select id="ruang_kelas_id" name="ruang_kelas_id" required>
                @foreach($ruangKelasCollection as $ruangKelas)
                    <option value="{{ $ruangKelas->id }}" {{ old('ruang_kelas_id') == $ruangKelas->id ? 'selected' : '' }}>{{$ruangKelas->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Mata Kuliah Ditawarkan</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Perbarui Mata Kuliah Ditawarkan -->
        <h2 style="color:#ef3b2d;">Perbarui Mata Kuliah Ditawarkan</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui mata kuliah ditawarkan. Field opsional yang dibiarkan akan dianggap bernilai sama dengan sebelumnya.</p>
        <form method="post" action="{{ route('perbarui-matkul-ditawarkan') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">ID Mata Kuliah Ditawarkan<sup>*</sup>:</label><br>
            <select id="id" name="id" required>
                @foreach($matkulDitawarkanCollection as $matkulDitawarkan)
                    <option value="{{ $matkulDitawarkan->id }}" {{ old('id') == $matkulDitawarkan->id ? 'selected' : '' }}>{{$matkulDitawarkan->id}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="mata_kuliah_id">Mata Kuliah:</label><br>
            <select id="mata_kuliah_id" name="mata_kuliah_id">
                <option value="">Pilih mata kuliah...</option>
                @foreach($namaMatkulCollection as $namaMatkul)
                    <option value="{{ $namaMatkul->id }}" {{ old('mata_kuliah_id') == $namaMatkul->id ? 'selected' : '' }}>{{$namaMatkul->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="dosen_id">Dosen:</label><br>
            <select id="dosen_id" name="dosen_id">
                <option value="">Pilih dosen...</option>
                @foreach($namaDosenCollection as $namaDosen)
                    <option value="{{ $namaDosen->id }}" {{ old('dosen_id') == $namaDosen->id ? 'selected' : '' }}>{{$namaDosen->nama}}</option>
                @endforeach
            </select><br>
            <label class="text-gray-600 dark:text-gray-400" for="ruang_kelas_id">Ruang Kelas:</label><br>
            <select id="ruang_kelas_id" name="ruang_kelas_id">
                <option value="">Pilih ruang...</option>
                @foreach($ruangKelasCollection as $ruangKelas)
                    <option value="{{ $ruangKelas->id }}" {{ old('ruang_kelas_id') == $ruangKelas->id ? 'selected' : '' }}>{{$ruangKelas->nama}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">Perbarui Mata Kuliah Ditawarkan</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        <!-- Form Hapus Mata Kuliah Ditawarkan -->
        <h2 style="color:#ef3b2d;">Hapus Mata Kuliah Ditawarkan</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus mata kuliah ditawarkan.</p>
        <form method="post" action="{{ route('hapus-matkul-ditawarkan') }}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="id">ID Mata Kuliah Ditawarkan<sup>*</sup>:</label><br>
            <select id="id" name="id" required>
                @foreach($matkulDitawarkanCollection as $matkulDitawarkan)
                    <option value="{{ $matkulDitawarkan->id }}" {{ old('id') == $matkulDitawarkan->id ? 'selected' : '' }}>{{$matkulDitawarkan->id}}</option>
                @endforeach
            </select><br>
            <button type="submit" class="nav next" style="margin-top:10px;">- Hapus Mata Kuliah Ditawarkan</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
    </div>
@stop
