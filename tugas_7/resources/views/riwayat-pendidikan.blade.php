@extends('layouts.default')
@section('content')
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0" style="padding: 1px 35px 35px;">

        <!-- Tombol ke Halaman-Halaman Lain -->
        <a href="{{route('daftar-dosen')}}" class="nav previous">&laquo; Daftar Dosen</a>
        <a href="{{route('daftar-mahasiswa')}}" class="nav next">Daftar Mahasiswa</a>
        <a href="{{route('mata-kuliah')}}" class="nav next">Daftar Mata Kuliah</a>
        <a href="{{route('ruang-kelas')}}" class="nav next">Daftar Ruang Kelas</a>

        <!-- Judul Halaman -->
        <h1 style="color:#EF3B2D;">Riwayat Pendidikan</h1>

        <!-- Menampilkan Error Jika Ada-->
        @if ($errors->any())
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="color: #f1f1f1;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Menampilkan Pesan Jika Ada-->
        @if (session()->has('message'))
            <div class="relative items-top justify-center sm:items-center py-4 sm:pt-0" style="padding-top:10px; padding-left:35px; margin-top:30px; background-color: #ef3b2d;">
                <h2 style="color: #f1f1f1;">Notifikasi</h2>
                <p style="color: #f1f1f1;">{{ session('message') }}</p>
            </div>
        @endif

        <!-- Paragraf Informasi Dosen -->
        <p class="text-gray-600 dark:text-gray-400">Nama: {{$dosen->nama . ", " . $dosen->gelar}}</p>
        <p class="text-gray-600 dark:text-gray-400">NIP: {{$dosen->nip}}</p>

        <!-- Tabel Riwayat Pendidikan -->
        @if ($riwayatPendidikanCollection->isNotEmpty())
            <table>
                <caption>Tabel Riwayat Pendidikan</caption>
                <tr>
                    <th id="ID">ID</th>
                    <th id="strata">Strata</th>
                    <th id="jurusan">Jurusan</th>
                    <th id="sekolah">Sekolah</th>
                    <th id="tahun mulai">Tahun Mulai</th>
                    <th id="tahun selesai">Tahun Selesai</th>
                </tr>
                @foreach ($riwayatPendidikanCollection as $riwayatPendidikan)
                    <tr>
                        <td>{{$riwayatPendidikan->id}}</td>
                        <td>{{$riwayatPendidikan->strata}}</td>
                        <td>{{$riwayatPendidikan->jurusan}}</td>
                        <td>{{$riwayatPendidikan->sekolah}}</td>
                        <td>{{$riwayatPendidikan->tahun_mulai}}</td>
                        <td>{{$riwayatPendidikan->tahun_selesai}}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="text-gray-600 dark:text-gray-400">Riwayat pendidikan belum dimasukkan. Silakan masukkan riwayat pendidikan.</p>
        @endif

        <!-- Form Tambahkan Riwayat Pendidikan -->
        <h2 style="color:#ef3b2d;">Tambahkan Riwayat Pendidikan</h2>
        <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menambahkan riwayat pendidikan.</p>
        <form method="post" action="{{route('tambahkan-riwayat')}}">
            @csrf
            <label class="text-gray-600 dark:text-gray-400" for="strata">Strata<sup>*</sup>:</label><br>
            <input type="text" id="strata" name="strata" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="jurusan">Jurusan<sup>*</sup>:</label><br>
            <input type="text" id="jurusan" name="jurusan" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="sekolah">Sekolah<sup>*</sup>:</label><br>
            <input type="text" id="sekolah" name="sekolah" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tahun_mulai">Tahun Mulai<sup>*</sup>:</label><br>
            <input type="number" id="tahun_mulai" name="tahun_mulai" required><br>
            <label class= "text-gray-600 dark:text-gray-400" for="tahun_selesai">Tahun Selesai<sup>*</sup>:</label><br>
            <input type="number" id="tahun_selesai" name="tahun_selesai" required><br>
            <button name="dosen_id" value="{{$dosen->id}}" type="submit" class="nav next" style="margin-top:10px;">+ Tambahkan Riwayat Pendidikan</button>
        </form>
        <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

        @if ($riwayatPendidikanCollection->isNotEmpty())
            <!-- Form Perbarui Riwayat Pendidikan -->
            <h2 style="color:#ef3b2d;">Perbarui Riwayat Pendidikan</h2>
            <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk memperbarui riwayat pendidikan.</p>
            <form method="post" action="{{route('perbarui-riwayat')}}">
                @csrf
                <label class="text-gray-600 dark:text-gray-400" for="id">ID:<sup>*</sup>:</label><br>
                <select id="id" name="id" required>
                    @foreach($riwayatPendidikanCollection as $riwayatPendidikan)
                        <option value="{{$riwayatPendidikan->id}}">{{$riwayatPendidikan->id}}</option>
                    @endforeach
                </select><br>
                <label class="text-gray-600 dark:text-gray-400" for="strata">Strata:</label><br>
                <input type="text" id="strata" name="strata"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="jurusan">Jurusan:</label><br>
                <input type="text" id="jurusan" name="jurusan"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="sekolah">Sekolah:</label><br>
                <input type="text" id="sekolah" name="sekolah"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="tahun_mulai">Tahun Mulai:</label><br>
                <input type="number" id="tahun_mulai" name="tahun_mulai"><br>
                <label class= "text-gray-600 dark:text-gray-400" for="tahun_selesai">Tahun Selesai:</label><br>
                <input type="number" id="tahun_selesai" name="tahun_selesai"><br>
                <button name="dosen_id" value="{{$dosen->id}}" type="submit" class="nav next" style="margin-top:10px;">Perbarui Riwayat Pendidikan</button>
            </form>
            <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>

            <!-- Form Hapus Riwayat Pendidikan -->
            <h2 style="color:#ef3b2d;">Hapus Riwayat Pendidikan</h2>
            <p class="text-gray-600 dark:text-gray-400">Gunakan form ini untuk menghapus riwayat pendidikan.</p>
            <form method="post" action="{{route('hapus-riwayat')}}">
                @csrf
                <label class="text-gray-600 dark:text-gray-400" for="id">ID:<sup>*</sup>:</label><br>
                <select id="id" name="id" required>
                    @foreach($riwayatPendidikanCollection as $riwayatPendidikan)
                        <option value="{{$riwayatPendidikan->id}}">{{$riwayatPendidikan->id}}</option>
                    @endforeach
                </select><br>
                <button name="dosen_id" value="{{$dosen->id}}" type="submit" class="nav next" style="margin-top:10px;">Hapus Riwayat Pendidikan</button>
            </form>
            <p class="text-gray-600 dark:text-gray-400">*) wajib diisi</p>
        @endif
    </div>
@stop
