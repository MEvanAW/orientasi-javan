<?php

namespace Modules\Mahasiswa\Models;

use App\Models\Krs;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Mahasiswa extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'mahasiswa';

    protected $guarded = [];

    protected $searchableColumns = ["nama", "nim", "jenis_kelamin", "tempat_lahir", "tanggal_lahir"];

    /**
     * Mendapatkan KRS yang dimiliki oleh si Mahasiswa
     */
    public function krs() {
        return $this->hasMany(Krs::class);
    }
}
