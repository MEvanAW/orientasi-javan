<?php

namespace Modules\Mahasiswa\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => ['required', 'max:255'],
            'nim' => ['required', 'max:255'],
            'jenis_kelamin' => ['required', 'max:10'],
            'tempat_lahir' => ['required', 'max:255'],
            'tanggal_lahir' => ['required', 'date', 'before:' . date("Y-m-d")]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasikan ACL
        return true;
    }
}
