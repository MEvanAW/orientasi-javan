<?php

use Modules\Mahasiswa\Controllers\MahasiswaController;

Route::group(
    [
        'prefix' => config('modules.mahasiswa.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.mahasiswa.route.middleware'),
    ],
    function () {
        Route::resource('mahasiswa', MahasiswaController::class);
    }
);
