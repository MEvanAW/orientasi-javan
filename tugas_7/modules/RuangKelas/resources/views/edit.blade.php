@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::ruang-kelas.index') }}"></x-backlink>

    <x-panel title="Edit Ruang Kelas">
        {!! form()->bind($ruangKelas)->put(route('modules::ruang-kelas.update', $ruangKelas->getKey()))->horizontal()->multipart() !!}
        @include('ruang-kelas::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
