@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::ruang-kelas.index') }}"></x-backlink>

    <x-panel title="Detil Ruang Kelas">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $ruangKelas->id }}</td></tr>
        <tr><td>Nama</td><td>{{ $ruangKelas->nama }}</td></tr>
        <tr><td>Created At</td><td>{{ $ruangKelas->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $ruangKelas->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
