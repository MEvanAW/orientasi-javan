@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Ruang Kelas">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::ruang-kelas.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
