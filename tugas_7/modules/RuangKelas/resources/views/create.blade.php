@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::ruang-kelas.index') }}"></x-backlink>

    <x-panel title="Tambah Ruang Kelas">
        {!! form()->post(route('modules::ruang-kelas.store'))->horizontal()->multipart() !!}
        @include('ruang-kelas::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
