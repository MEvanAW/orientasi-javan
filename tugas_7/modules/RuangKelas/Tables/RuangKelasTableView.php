<?php

namespace Modules\RuangKelas\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\RuangKelas\Models\RuangKelas;

class RuangKelasTableView extends TableView
{
    public function source()
    {
        return RuangKelas::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama')->sortable(),
            RestfulButton::make('modules::ruang-kelas'),
        ];
    }
}
