<?php

use Modules\RuangKelas\Controllers\RuangKelasController;

Route::group(
    [
        'prefix' => config('modules.ruang-kelas.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.ruang-kelas.route.middleware'),
    ],
    function () {
        Route::resource('ruang-kelas', RuangKelasController::class);
    }
);
