<?php

namespace Modules\RuangKelas\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class RuangKelas extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'ruang_kelas';

    protected $guarded = [];

    protected $searchableColumns = ["nama",];
}
