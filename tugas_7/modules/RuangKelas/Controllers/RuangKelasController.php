<?php

namespace Modules\RuangKelas\Controllers;

use Illuminate\Routing\Controller;
use Modules\RuangKelas\Requests\Store;
use Modules\RuangKelas\Requests\Update;
use Modules\RuangKelas\Models\RuangKelas;
use Modules\RuangKelas\Tables\RuangKelasTableView;

class RuangKelasController extends Controller
{
    public function index()
    {
        return RuangKelasTableView::make()->view('ruang-kelas::index');
    }

    public function create()
    {
        return view('ruang-kelas::create');
    }

    public function store(Store $request)
    {
        RuangKelas::create($request->validated());

        return redirect()->back()->withSuccess('Ruang Kelas saved');
    }

    public function show(RuangKelas $ruangKelas)
    {
        return view('ruang-kelas::show', compact('ruangKelas'));
    }

    public function edit(RuangKelas $ruangKelas)
    {
        return view('ruang-kela::edit', compact('ruangKelas'));
    }

    public function update(Update $request, RuangKelas $ruangKelas)
    {
        $ruangKelas->update($request->validated());

        return redirect()->back()->withSuccess('Ruang Kelas saved');
    }

    public function destroy(RuangKelas $ruangKelas)
    {
        $ruangKelas->delete();

        return redirect()->back()->withSuccess('Ruang Kelas deleted');
    }
}
