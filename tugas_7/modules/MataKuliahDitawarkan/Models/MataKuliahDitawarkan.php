<?php

namespace Modules\MataKuliahDitawarkan\Models;

use App\Models\Dosen;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\MataKuliah\Models\MataKuliah;
use Modules\RuangKelas\Models\RuangKelas;

class MataKuliahDitawarkan extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'mata_kuliah_ditawarkan';

    protected $guarded = [];

    protected $searchableColumns = [];

    /**
     * Dapatkan model mata kuliah terkait
     */
    public function mataKuliah() {
        return $this->belongsTo(MataKuliah::class);
    }

    /**
     * Dapatkan dosen pengajar
     */
    public function dosen() {
        return $this->belongsTo(Dosen::class);
    }

    /**
     * Dapatkan ruang kelas tempat mata kuliah diselenggarakan.
     */
    public function ruangKelas() {
        return $this->belongsTo(RuangKelas::class);
    }
}
