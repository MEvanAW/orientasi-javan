<?php

namespace Modules\MataKuliahDitawarkan\Controllers;

use Illuminate\Routing\Controller;
use Modules\MataKuliahDitawarkan\Requests\Store;
use Modules\MataKuliahDitawarkan\Requests\Update;
use Modules\MataKuliahDitawarkan\Models\MataKuliahDitawarkan;
use Modules\MataKuliahDitawarkan\Tables\MataKuliahDitawarkanTableView;

class MataKuliahDitawarkanController extends Controller
{
    public function index()
    {
        return MataKuliahDitawarkanTableView::make()->view('mata-kuliah-ditawarkan::index');
    }

    public function create()
    {
        return view('mata-kuliah-ditawarkan::create');
    }

    public function store(Store $request)
    {
        MataKuliahDitawarkan::create($request->validated());

        return redirect()->back()->withSuccess('Mata Kuliah Ditawarkan saved');
    }

    public function show(MataKuliahDitawarkan $mataKuliahDitawarkan)
    {
        return view('mata-kuliah-ditawarkan::show', compact('mataKuliahDitawarkan'));
    }

    public function edit(MataKuliahDitawarkan $mataKuliahDitawarkan)
    {
        return view('mata-kuliah-ditawarkan::edit', compact('mataKuliahDitawarkan'));
    }

    public function update(Update $request, MataKuliahDitawarkan $mataKuliahDitawarkan)
    {
        $mataKuliahDitawarkan->update($request->validated());

        return redirect()->back()->withSuccess('Mata Kuliah Ditawarkan saved');
    }

    public function destroy(MataKuliahDitawarkan $mataKuliahDitawarkan)
    {
        $mataKuliahDitawarkan->delete();

        return redirect()->back()->withSuccess('Mata Kuliah Ditawarkan deleted');
    }
}
