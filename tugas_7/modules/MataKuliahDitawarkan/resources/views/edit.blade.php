@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah-ditawarkan.index') }}"></x-backlink>

    <x-panel title="Edit Mata Kuliah Ditawarkan">
        {!! form()->bind($mataKuliahDitawarkan)->put(route('modules::mata-kuliah-ditawarkan.update', $mataKuliahDitawarkan->getKey()))->horizontal()->multipart() !!}
        @include('mata-kuliah-ditawarkan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
