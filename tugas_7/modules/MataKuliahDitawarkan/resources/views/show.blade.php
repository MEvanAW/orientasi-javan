@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah-ditawarkan.index') }}"></x-backlink>

    <x-panel title="Detil Mata Kuliah Ditawarkan">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $mataKuliahDitawarkan->id }}</td></tr>
        <tr><td>Mata Kuliah Id</td><td>{{ $mataKuliahDitawarkan->mata_kuliah_id }}</td></tr>
        <tr><td>Dosen Id</td><td>{{ $mataKuliahDitawarkan->dosen_id }}</td></tr>
        <tr><td>Ruang Kelas Id</td><td>{{ $mataKuliahDitawarkan->ruang_kelas_id }}</td></tr>
        <tr><td>Created At</td><td>{{ $mataKuliahDitawarkan->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $mataKuliahDitawarkan->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
