@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah-ditawarkan.index') }}"></x-backlink>

    <x-panel title="Tambah Mata Kuliah Ditawarkan">
        {!! form()->post(route('modules::mata-kuliah-ditawarkan.store'))->horizontal()->multipart() !!}
        @include('mata-kuliah-ditawarkan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
