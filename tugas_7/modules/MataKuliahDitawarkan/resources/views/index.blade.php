@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Mata Kuliah Ditawarkan">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::mata-kuliah-ditawarkan.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
