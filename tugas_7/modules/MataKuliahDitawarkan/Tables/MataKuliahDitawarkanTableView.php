<?php

namespace Modules\MataKuliahDitawarkan\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\MataKuliahDitawarkan\Models\MataKuliahDitawarkan;

class MataKuliahDitawarkanTableView extends TableView
{
    public function source()
    {
        return MataKuliahDitawarkan::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('mata_kuliah_id')->sortable(),
            Text::make('dosen_id')->sortable(),
            Text::make('ruang_kelas_id')->sortable(),
            RestfulButton::make('modules::mata-kuliah-ditawarkan'),
        ];
    }
}
