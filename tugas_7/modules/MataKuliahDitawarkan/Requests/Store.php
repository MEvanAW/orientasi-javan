<?php

namespace Modules\MataKuliahDitawarkan\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mata_kuliah_id' => ['required', 'numeric', 'exists:mata_kuliah,id'],
            'dosen_id' => ['required', 'numeric', 'exists:dosen,id'],
            'ruang_kelas_id' => ['required', 'numeric', 'exists:ruang_kelas,id'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasikan ACL
        return true;
    }
}
