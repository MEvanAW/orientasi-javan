<?php

use Modules\MataKuliahDitawarkan\Controllers\MataKuliahDitawarkanController;

Route::group(
    [
        'prefix' => config('modules.mata-kuliah-ditawarkan.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.mata-kuliah-ditawarkan.route.middleware'),
    ],
    function () {
        Route::resource('mata-kuliah-ditawarkan', MataKuliahDitawarkanController::class);
    }
);
