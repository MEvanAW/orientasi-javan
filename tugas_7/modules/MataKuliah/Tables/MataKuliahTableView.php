<?php

namespace Modules\MataKuliah\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\MataKuliah\Models\MataKuliah;

class MataKuliahTableView extends TableView
{
    public function source()
    {
        return MataKuliah::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama')->sortable(),
            Text::make('SKS')->sortable(),
            RestfulButton::make('modules::mata-kuliah'),
        ];
    }
}
