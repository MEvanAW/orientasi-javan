<?php

namespace Modules\MataKuliah\Controllers;

use Illuminate\Routing\Controller;
use Modules\MataKuliah\Requests\Store;
use Modules\MataKuliah\Requests\Update;
use Modules\MataKuliah\Models\MataKuliah;
use Modules\MataKuliah\Tables\MataKuliahTableView;

class MataKuliahController extends Controller
{
    public function index()
    {
        return MataKuliahTableView::make()->view('mata-kuliah::index');
    }

    public function create()
    {
        return view('mata-kuliah::create');
    }

    public function store(Store $request)
    {
        MataKuliah::create($request->validated());

        return redirect()->back()->withSuccess('Mata Kuliah saved');
    }

    public function show(MataKuliah $mataKuliah)
    {
        return view('mata-kuliah::show', compact('mataKuliah'));
    }

    public function edit(MataKuliah $mataKuliah)
    {
        return view('mata-kuliah::edit', compact('mataKuliah'));
    }

    public function update(Update $request, MataKuliah $mataKuliah)
    {
        $mataKuliah->update($request->validated());

        return redirect()->back()->withSuccess('Mata Kuliah saved');
    }

    public function destroy(MataKuliah $mataKuliah)
    {
        $mataKuliah->delete();

        return redirect()->back()->withSuccess('Mata Kuliah deleted');
    }
}
