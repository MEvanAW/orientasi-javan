@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah.index') }}"></x-backlink>

    <x-panel title="Tambah Mata Kuliah">
        {!! form()->post(route('modules::mata-kuliah.store'))->horizontal()->multipart() !!}
        @include('mata-kuliah::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
