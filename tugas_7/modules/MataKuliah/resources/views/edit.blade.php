@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah.index') }}"></x-backlink>

    <x-panel title="Edit Mata Kuliah">
        {!! form()->bind($mataKuliah)->put(route('modules::mata-kuliah.update', $mataKuliah->getKey()))->horizontal()->multipart() !!}
        @include('mata-kuliah::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
