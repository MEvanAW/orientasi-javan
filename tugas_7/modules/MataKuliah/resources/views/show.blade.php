@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mata-kuliah.index') }}"></x-backlink>

    <x-panel title="Detil Mata Kuliah">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $mataKuliah->id }}</td></tr>
        <tr><td>Nama</td><td>{{ $mataKuliah->nama }}</td></tr>
        <tr><td>Sks</td><td>{{ $mataKuliah->SKS }}</td></tr>
        <tr><td>Created At</td><td>{{ $mataKuliah->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $mataKuliah->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
