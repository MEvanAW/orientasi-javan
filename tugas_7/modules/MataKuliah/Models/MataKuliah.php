<?php

namespace Modules\MataKuliah\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class MataKuliah extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'mata_kuliah';

    protected $guarded = [];

    protected $searchableColumns = ["nama", "SKS",];
}
