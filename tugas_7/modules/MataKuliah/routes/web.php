<?php

use Modules\MataKuliah\Controllers\MataKuliahController;

Route::group(
    [
        'prefix' => config('modules.mata-kuliah.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.mata-kuliah.route.middleware'),
    ],
    function () {
        Route::resource('mata-kuliah', MataKuliahController::class);
    }
);
