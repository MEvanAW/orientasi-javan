<?php

use Modules\RiwayatPendidikan\Controllers\RiwayatPendidikanController;

Route::group(
    [
        'prefix' => config('modules.riwayat-pendidikan.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.riwayat-pendidikan.route.middleware'),
    ],
    function () {
        Route::resource('riwayat-pendidikan', RiwayatPendidikanController::class);
    }
);
