<?php

namespace Modules\RiwayatPendidikan\Models;

use App\Models\Dosen;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class RiwayatPendidikan extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'riwayat_pendidikan';

    protected $guarded = [];

    protected $searchableColumns = ["strata", "jurusan", "sekolah", "tahun_mulai", "tahun_selesai",];

    /**
     * Mendapatkan dosen pemilik riwayat pendidikan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dosen() {
        return $this->belongsTo(Dosen::class);
    }
}
