@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::riwayat-pendidikan.index') }}"></x-backlink>

    <x-panel title="Tambah Riwayat Pendidikan">
        {!! form()->post(route('modules::riwayat-pendidikan.store'))->horizontal()->multipart() !!}
        @include('riwayat-pendidikan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
