@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::riwayat-pendidikan.index') }}"></x-backlink>

    <x-panel title="Detil Riwayat Pendidikan">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $riwayatPendidikan->id }}</td></tr>
        <tr><td>Dosen Id</td><td>{{ $riwayatPendidikan->dosen_id }}</td></tr>
        <tr><td>Strata</td><td>{{ $riwayatPendidikan->strata }}</td></tr>
        <tr><td>Jurusan</td><td>{{ $riwayatPendidikan->jurusan }}</td></tr>
        <tr><td>Sekolah</td><td>{{ $riwayatPendidikan->sekolah }}</td></tr>
        <tr><td>Tahun Mulai</td><td>{{ $riwayatPendidikan->tahun_mulai }}</td></tr>
        <tr><td>Tahun Selesai</td><td>{{ $riwayatPendidikan->tahun_selesai }}</td></tr>
        <tr><td>Created At</td><td>{{ $riwayatPendidikan->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $riwayatPendidikan->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
