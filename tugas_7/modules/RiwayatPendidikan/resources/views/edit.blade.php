@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::riwayat-pendidikan.index') }}"></x-backlink>

    <x-panel title="Edit Riwayat Pendidikan">
        {!! form()->bind($riwayatPendidikan)->put(route('modules::riwayat-pendidikan.update', $riwayatPendidikan->getKey()))->horizontal()->multipart() !!}
        @include('riwayat-pendidikan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
