<?php

namespace Modules\RiwayatPendidikan\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\RiwayatPendidikan\Models\RiwayatPendidikan;

class RiwayatPendidikanTableView extends TableView
{
    public function source()
    {
        return RiwayatPendidikan::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('dosen_id')->sortable(),
            Text::make('strata')->sortable(),
            Text::make('jurusan')->sortable(),
            Text::make('sekolah')->sortable(),
            Text::make('tahun_mulai')->sortable(),
            Text::make('tahun_selesai')->sortable(),
            RestfulButton::make('modules::riwayat-pendidikan'),
        ];
    }
}
