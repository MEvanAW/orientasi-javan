<?php

namespace Modules\RiwayatPendidikan\Controllers;

use Illuminate\Routing\Controller;
use Modules\RiwayatPendidikan\Requests\Store;
use Modules\RiwayatPendidikan\Requests\Update;
use Modules\RiwayatPendidikan\Models\RiwayatPendidikan;
use Modules\RiwayatPendidikan\Tables\RiwayatPendidikanTableView;

class RiwayatPendidikanController extends Controller
{
    public function index()
    {
        return RiwayatPendidikanTableView::make()->view('riwayat-pendidikan::index');
    }

    public function create()
    {
        return view('riwayat-pendidikan::create');
    }

    public function store(Store $request)
    {
        RiwayatPendidikan::create($request->validated());

        return redirect()->back()->withSuccess('Riwayat Pendidikan saved');
    }

    public function show(RiwayatPendidikan $riwayatPendidikan)
    {
        return view('riwayat-pendidikan::show', compact('riwayatPendidikan'));
    }

    public function edit(RiwayatPendidikan $riwayatPendidikan)
    {
        return view('riwayat-pendidikan::edit', compact('riwayatPendidikan'));
    }

    public function update(Update $request, RiwayatPendidikan $riwayatPendidikan)
    {
        $riwayatPendidikan->update($request->validated());

        return redirect()->back()->withSuccess('Riwayat Pendidikan saved');
    }

    public function destroy(RiwayatPendidikan $riwayatPendidikan)
    {
        $riwayatPendidikan->delete();

        return redirect()->back()->withSuccess('Riwayat Pendidikan deleted');
    }
}
