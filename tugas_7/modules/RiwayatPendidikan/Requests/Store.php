<?php

namespace Modules\RiwayatPendidikan\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dosen_id' => ['required'],
            'strata' => ['required'],
            'jurusan' => ['required'],
            'sekolah' => ['required'],
            'tahun_mulai' => ['required'],
            'tahun_selesai' => ['required'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
