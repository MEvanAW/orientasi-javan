<?php

use App\Http\Controllers\Dashboard;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\KrsController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MataKuliahController;
use App\Http\Controllers\RiwayatPendidikanController;
use App\Http\Controllers\RuangKelasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rute-Rute Get
Route::get('/', function () {
    return view('welcome');
});
Route::get('/daftar-dosen', [DosenController::class, 'daftarDosen'])->name('daftar-dosen');
Route::get('/riwayat-pendidikan/{dosenId}', [RiwayatPendidikanController::class, 'riwayatPendidikan'])->name('riwayat-pendidikan');
Route::get('/mahasiswa', [MahasiswaController::class, 'daftarMahasiswa'])->name('daftar-mahasiswa');
Route::get('/mahasiswa/{mahasiswaId}/krs', [KrsController::class, 'krs'])->name('krs');
Route::get('/ruang-kelas', [RuangKelasController::class, 'ruangKelas'])->name('ruang-kelas');
Route::get('/mata-kuliah', [MataKuliahController::class, 'mataKuliah'])->name('mata-kuliah');
Route::get('/mata-kuliah/ditawarkan', [MataKuliahController::class, 'matkulDitawarkan'])->name('matkul-ditawarkan');
Route::get('/dashboard', Dashboard::class)->name('dashboard')->middleware('auth');

// Rute-Rute Pos
Route::post('/tambahkan-dosen', [DosenController::class, 'tambahkanDosen'])->name('tambahkan-dosen');
Route::post('/perbarui-dosen', [DosenController::class, 'perbaruiDosen'])->name('perbarui-dosen');
Route::post('/hapus-dosen', [DosenController::class, 'hapusDosen'])->name('hapus-dosen');
Route::post('/tambahkan-riwayat-pendidikan', [RiwayatPendidikanController::class, 'tambahkanRiwayat'])->name('tambahkan-riwayat');
Route::post('/perbarui-riwayat-pendidikan', [RiwayatPendidikanController::class, 'perbaruiRiwayat'])->name('perbarui-riwayat');
Route::post('/hapus-riwayat-pendidikan', [RiwayatPendidikanController::class, 'hapusRiwayat'])->name('hapus-riwayat');
Route::post('/tambahkan-mahasiswa', [MahasiswaController::class, 'tambahkanMahasiswa'])->name('tambahkan-mahasiswa');
Route::post('/perbarui-mahasiswa', [MahasiswaController::class, 'perbaruiMahasiswa'])->name('perbarui-mahasiswa');
Route::post('/hapus-mahasiswa', [MahasiswaController::class, 'hapusMahasiswa'])->name('hapus-mahasiswa');
Route::post('/tambahkan-ruang-kelas', [RuangKelasController::class, 'tambahkanRuang'])->name('tambahkan-ruang');
Route::post('/perbarui-ruang-kelas', [RuangKelasController::class, 'perbaruiRuang'])->name('perbarui-ruang');
Route::post('/hapus-ruang-kelas', [RuangKelasController::class, 'hapusRuang'])->name('hapus-ruang');
Route::post('/tambahkan-mata-kuliah', [MataKuliahController::class, 'tambahkanMatkul'])->name('tambahkan-matkul');
Route::post('/perbarui-mata-kuliah', [MataKuliahController::class, 'perbaruiMatkul'])->name('perbarui-matkul');
Route::post('/hapus-mata-kuliah', [MataKuliahController::class, 'hapusMatkul'])->name('hapus-matkul');
Route::post('/mata-kuliah/ditawarkan/store', [MataKuliahController::class, 'tambahkanMatkulDitawarkan'])->name('tambahkan-matkul-ditawarkan');
Route::post('/mata-kuliah/ditawarkan/update', [MataKuliahController::class, 'perbaruiMatkulDitawarkan'])->name('perbarui-matkul-ditawarkan');
Route::post('/mata-kuliah/ditawarkan/delete', [MataKuliahController::class, 'hapusMatkulDitawarkan'])->name('hapus-matkul-ditawarkan');
Route::post('/mahasiswa/krs/store', [KrsController::class, 'ambilMatkul'])->name('ambil-matkul');
Route::post('/mahasiswa/krs/delete', [KrsController::class, 'batalkanPengambilan'])->name('batalkan-pengambilan');
