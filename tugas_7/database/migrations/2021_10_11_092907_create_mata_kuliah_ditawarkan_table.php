<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMataKuliahTersediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah_ditawarkan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mata_kuliah_id');
            $table->unsignedBigInteger('dosen_id');
            $table->unsignedBigInteger('ruang_kelas_id');
            $table->timestamps();
            $table->foreign('mata_kuliah_id')
                ->references('id')
                ->on('mata_kuliah')
                ->onUpdate('cascade');
            $table->foreign('dosen_id')
                ->references('id')
                ->on('dosen')
                ->onUpdate('cascade');
            $table->foreign('ruang_kelas_id')
                ->references('id')
                ->on('ruang_kelas')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_kuliah_ditawarkan');
    }
}
