<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RuangKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ruang_kelas')->insert([
            /*1*/ ['nama' => 'E5',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'E6',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
