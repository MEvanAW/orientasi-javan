<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswa')->insert([
            /*1*/ ['nama' => 'Muhammad Evan Anindya Wahyuaji', 'nim' => '18/425316/TK/47011', 'jenis_kelamin' => 'laki-laki', 'tempat_lahir' => 'Klaten, Jawa Tengah', 'tanggal_lahir' => '1999-07-09',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Albertus Magnus Arya Baskara', 'nim' => '15/379906/TK/43171', 'jenis_kelamin' => 'laki-laki', 'tempat_lahir' => 'Sleman, Daerah Istimewa Yogyakarta', 'tanggal_lahir' => '1996-12-31',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
