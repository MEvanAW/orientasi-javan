<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dosen')->insert([
            /*1*/ ['nama' => 'Prof. Ir. Radianta Triatmadja', 'nip' => 'FT1', 'gelar' => 'Ph.D.',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Dr. Indarsih', 'nip' => 'FMIPA1', 'gelar' => 'S.Si., M.Si.', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
