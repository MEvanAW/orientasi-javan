<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MataKuliahDitawarkanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mata_kuliah_ditawarkan')->insert([
            /*1*/ ['mata_kuliah_id' => 1, 'dosen_id' => 1, 'ruang_kelas_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['mata_kuliah_id' => 1, 'dosen_id' => 2, 'ruang_kelas_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
