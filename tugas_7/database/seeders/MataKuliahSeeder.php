<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mata_kuliah')->insert([
            /*1*/ ['nama' => 'Pengalaman Pengguna',  'SKS' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['nama' => 'Kewarganegaraan',  'SKS' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
