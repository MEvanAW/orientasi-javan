<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RiwayatPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('riwayat_pendidikan')->insert([
            /*1*/ ['dosen_id' => 1, 'strata' => 'Doktor', 'jurusan' => 'Teknik Sipil', 'sekolah' => 'Strathlyde University, United Kingdom', 'tahun_mulai' => 1987, 'tahun_selesai' => 1990, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            /*2*/ ['dosen_id' => 1, 'strata' => 'Sarjana', 'jurusan' => 'Teknik Sipil', 'sekolah' => 'Universitas Gadjah Mada, Indonesia', 'tahun_mulai' => 1978, 'tahun_selesai' => 1984, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
