<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\MataKuliahDitawarkan\Models\MataKuliahDitawarkan;

class Krs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'krs';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'mahasiswa_id',
        'mata_kuliah_ditawarkan_id'
    ];

    public function mataKuliahDitawarkan() {
        return $this->belongsTo(MataKuliahDitawarkan::class);
    }
}
