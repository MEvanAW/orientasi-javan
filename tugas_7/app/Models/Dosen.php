<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\RiwayatPendidikan\Models\RiwayatPendidikan;

class Dosen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dosen';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nama',
        'nip',
        'gelar'
    ];

    /**
     * Mendapatkan riwayat pendidikan dosen.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayatPendidikan() {
        return $this->hasMany(RiwayatPendidikan::class);
    }
}
