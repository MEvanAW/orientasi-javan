<?php

namespace App\Http\Controllers;

use App\Http\Requests\HapusMahasiswaRequest;
use App\Http\Requests\PerbaruiMahasiswaRequest;
use Illuminate\Support\Facades\DB;
use Modules\Mahasiswa\Models\Mahasiswa;
use Modules\Mahasiswa\Requests\Store;
use Throwable;

class MahasiswaController extends Controller
{
    /**
     * Fungsi untuk mengembalikan view Daftar Mahasiswa.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function daftarMahasiswa() {
        $mahasiswaCollection = Mahasiswa::all();
        return view('daftar-mahasiswa', compact('mahasiswaCollection'));
    }

    /**
     * Fungsi untuk menambahkan seorang Mahasiswa.
     *
     * @param Store $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tambahkanMahasiswa(Store $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $mahasiswa = new Mahasiswa;
        try {
            $mahasiswa->id = DB::table('mahasiswa')->find(DB::table('mahasiswa')->max('id'))->id + 1;
            $mahasiswa->nama = strip_tags($validated['nama']);
            $mahasiswa->nim = strip_tags($validated['nim']);
            $mahasiswa->jenis_kelamin = strip_tags($validated['jenis_kelamin']);
            $mahasiswa->tempat_lahir = strip_tags($validated['tempat_lahir']);
            $mahasiswa->tanggal_lahir = $validated['tanggal_lahir'];
            $mahasiswa->save();
        } catch (Throwable $e) {
            return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, penambahan mahasiswa ' . $validated['nama'] . ' terjadi error: ' . $e->getMessage());
        }
        return redirect(route('daftar-mahasiswa'))->with('message', 'Mahasiswa ' . $validated['nama'] . ' berhasil ditambahkan.');
    }

    /**
     * Fungsi untuk memperbarui data seorang Mahasiswa.
     *
     * @param PerbaruiMahasiswaRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiMahasiswa(PerbaruiMahasiswaRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $mahasiswa = Mahasiswa::find($validated['id']);
            if (!empty($validated['nama'])) {
                $mahasiswa->nama = strip_tags($validated['nama']);
            }
            if (!empty($validated['nim'])) {
                $mahasiswa->nim = strip_tags($validated['nim']);
            }
            if (!empty($validated['jenis_kelamin'])) {
                $mahasiswa->jenis_kelamin = strip_tags($validated['jenis_kelamin']);
            }
            if (!empty($validated['tempat_lahir'])) {
                $mahasiswa->tempat_lahir = $validated['tempat_lahir'];
            }
            if (!empty($validated['tanggal_lahir'])) {
                $mahasiswa->tanggal_lahir = $validated['tanggal_lahir'];
            }
            $mahasiswa->save();
        } catch (Throwable $e) {
            if (isset($mahasiswa)) {
                return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, pembaruan data mahasiswa ' . $mahasiswa->nama . ' gagal dengan error: ' . $e->getMessage());
            } else {
                return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, pembaruan data mahasiswa gagal dengan error: ' . $e->getMessage());
            }
        }
        return redirect(route('daftar-mahasiswa'))->with('message', 'Data mahasiswa ' . $mahasiswa->nama . ' berhasil diperbarui.');
    }

    /**
     * Sebuah fungsi untuk menghapus data seorang Mahasiswa.
     *
     * @param HapusMahasiswaRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusMahasiswa(HapusMahasiswaRequest $request) {
        $validated = $request->validated();
        $nama = " ";
        try {
            $mahasiswa = Mahasiswa::find($validated['id']);
            $nama = $nama . $mahasiswa->nama . " ";
            $mahasiswa->delete();
        } catch (Throwable $e) {
            return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, data mahasiswa' . $nama . 'gagal dihapus dengan error: ' . $e->getMessage());
        }
        return redirect(route('daftar-mahasiswa'))->with('message', 'Data mahasiswa' . $nama . 'berhasil dihapus.');
    }
}
