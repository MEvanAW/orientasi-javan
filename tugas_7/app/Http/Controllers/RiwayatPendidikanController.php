<?php

namespace App\Http\Controllers;

use App\Http\Requests\HapusRiwayatRequest;
use App\Http\Requests\PerbaruiRiwayatRequest;
use App\Http\Requests\TambahkanRiwayatRequest;
use App\Models\Dosen;
use Illuminate\Support\Facades\DB;
use Modules\RiwayatPendidikan\Models\RiwayatPendidikan;
use Throwable;

class RiwayatPendidikanController extends Controller
{
    /**
     * Mengembalikan view Riwayat Pendidikan
     *
     * @param $dosenId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function riwayatPendidikan($dosenId) {
        if (!is_numeric($dosenId)) {
            return redirect(route('daftar-dosen'))->with('message', 'Mohon maaf, tautan riwayat pendidikan salah.');
        }
        try {
            $dosen = Dosen::find($dosenId);
            if ($dosen != null) {
                $riwayatPendidikanCollection = $dosen->riwayatPendidikan;
            }
        } catch (Throwable $e) {
            return redirect(route('daftar-dosen'))->with('message', 'Mohon maaf, terjadi error dengan pesan: ' . $e->getMessage());
        }
        if ($dosen == null) {
            return redirect(route('daftar-dosen'))->with('message', 'Mohon maaf, tautan riwayat pendidikan salah.');
        }
        return view('riwayat-pendidikan', compact('dosen', 'riwayatPendidikanCollection'));
    }

    /**
     * Fungsi untuk menambahkan butir Riwayat Pendidikan.
     *
     * @param TambahkanRiwayatRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tambahkanRiwayat(TambahkanRiwayatRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $riwayatPendidikan = new RiwayatPendidikan;
        try {
            $riwayatPendidikan->id = DB::table('riwayat_pendidikan')->find(DB::table('riwayat_pendidikan')->max('id'))->id + 1;
            $riwayatPendidikan->dosen_id = $validated['dosen_id'];
            $riwayatPendidikan->strata = strip_tags($validated['strata']);
            $riwayatPendidikan->jurusan = strip_tags($validated['jurusan']);
            $riwayatPendidikan->sekolah = strip_tags($validated['sekolah']);
            $riwayatPendidikan->tahun_mulai = $validated['tahun_mulai'];
            $riwayatPendidikan->tahun_selesai = $validated['tahun_selesai'];
            $riwayatPendidikan->save();
        } catch (Throwable $e) {
            return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Mohon maaf, penambahan riwayat pendidikan terjadi error: ' . $e->getMessage());
        }
        return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Riwayat pendidikan berhasil ditambahkan.');
    }

    /**
     * Fungsi untuk memperbarui butir Riwayat Pendidikan.
     *
     * @param PerbaruiRiwayatRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiRiwayat(PerbaruiRiwayatRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $riwayatPendidikan = RiwayatPendidikan::find($validated['id']);
            if (!empty($validated['strata'])) {
                $riwayatPendidikan->strata = strip_tags($validated['strata']);
            }
            if (!empty($validated['jurusan'])) {
                $riwayatPendidikan->jurusan = strip_tags($validated['jurusan']);
            }
            if (!empty($validated['sekolah'])) {
                $riwayatPendidikan->sekolah = strip_tags($validated['sekolah']);
            }
            if (!empty($validated['tahun_mulai'])) {
                $riwayatPendidikan->tahun_mulai = $validated['tahun_mulai'];
            }
            if (!empty($validated['tahun_selesai'])) {
                $riwayatPendidikan->tahun_selesai = $validated['tahun_selesai'];
            }
            $riwayatPendidikan->save();
        } catch (Throwable $e) {
            if (isset($riwayatPendidikan)) {
                return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Mohon maaf, pembaruan riwayat pendidikan dengan ID = ' . $riwayatPendidikan->id . ' gagal dengan error: ' . $e->getMessage());
            } else {
                return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Mohon maaf, pembaruan riwayat pendidikan gagal dengan error: ' . $e->getMessage());
            }
        }
        return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Riwayat pendidikan dengan ID = ' . $riwayatPendidikan->id . ' berhasil diperbarui.');
    }

    /**
     * Fungsi untuk menghapus butir Riwayat Pendidikan.
     *
     * @param HapusRiwayatRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusRiwayat(HapusRiwayatRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $riwayatPendidikan = RiwayatPendidikan::find($validated['id']);
            $riwayatPendidikan->delete();
        } catch (Throwable $e) {
            return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Riwayat pendidikan dengan ID = ' . $validated['id'] . ' gagal dihapus dengan error: ' . $e->getMessage());
        }
        return redirect(route('riwayat-pendidikan', ['dosenId' => $validated['dosen_id']]))->with('message', 'Riwayat pendidikan dengan ID = ' . $validated['id'] . ' berhasil dihapus.');
    }
}
