<?php

namespace App\Http\Controllers;

use App\Http\Requests\HapusMatkulDitawarkanRequest;
use App\Http\Requests\HapusMatkulRequest;
use App\Http\Requests\PerbaruiMatkulDitawarkanRequest;
use App\Http\Requests\PerbaruiMatkulRequest;
use App\Models\Dosen;
use Illuminate\Support\Facades\DB;
use Modules\MataKuliah\Models\MataKuliah;
use Modules\MataKuliah\Requests\Store;
use Modules\MataKuliahDitawarkan\Models\MataKuliahDitawarkan;
use Modules\RuangKelas\Models\RuangKelas;
use Throwable;

class MataKuliahController extends Controller
{
    /**
     * Fungsi untuk mengembalikan view Daftar Mata Kuliah.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function mataKuliah() {
        try {
            $mataKuliahCollection = MataKuliah::all();
        } catch (Throwable $e) {
            return view('welcome')->with('message', 'Mohon maaf, terjadi error memuat mata kuliah: ' . $e->getMessage());
        }
        return view('mata-kuliah', compact('mataKuliahCollection'));
    }

    /**
     * Fungsi untuk mengembalikan view Mata Kuliah Ditawarkan.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function matkulDitawarkan() {
        try {
            $matkulDitawarkanCollection = MataKuliahDitawarkan::all();
            $namaMatkulCollection = MataKuliah::select('id', 'nama')->get();
            $namaDosenCollection = Dosen::select('id', 'nama')->get();
            $ruangKelasCollection = RuangKelas::all();
        } catch (Throwable $e) {
            return view ('welcome')->with('message', 'Mohon maaf, terjadi error dalam memuat mata kuliah terselenggara: ' . $e->getMessage());
        }
        return view('matkul-ditawarkan', compact('matkulDitawarkanCollection', 'namaMatkulCollection', 'namaDosenCollection', 'ruangKelasCollection'));
    }

    /**
     * Fungsi untuk menambahkan sebuah Mata Kuliah.
     *
     * @param Store $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tambahkanMatkul(Store $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $mataKuliah = new MataKuliah;
        try {
            $mataKuliah->id = DB::table('mata_kuliah')->find(DB::table('mata_kuliah')->max('id'))->id + 1;
            $mataKuliah->nama = strip_tags($validated['nama']);
            $mataKuliah->SKS = $validated['SKS'];
            $mataKuliah->save();
        } catch (Throwable $e) {
            return redirect(route('mata-kuliah'))->with('message', 'Mohon maaf, penambahan mata kuliah ' . $validated['nama'] . ' terjadi error: ' . $e->getMessage());
        }
        return redirect(route('mata-kuliah'))->with('message', 'Mata kuliah ' . $validated['nama'] . ' berhasil ditambahkan.');
    }

    /**
     * Fungsi untuk memperbarui sebuah Mata Kuliah.
     *
     * @param PerbaruiMatkulRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiMatkul(PerbaruiMatkulRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $mataKuliah = MataKuliah::find($validated['id']);
            if (!empty($validated['nama'])) {
                $mataKuliah->nama = strip_tags($validated['nama']);
            }
            if (!empty($validated['SKS'])) {
                $mataKuliah->SKS = $validated['SKS'];
            }
            $mataKuliah->save();
        } catch (Throwable $e) {
            if (isset($mataKuliah)) {
                return redirect(route('mata-kuliah'))->with('message', 'Mohon maaf, pembaruan mata kuliah' . $mataKuliah->nama . ' gagal dengan error: ' . $e->getMessage());
            } else {
                return redirect(route('mata-kuliah'))->with('message', 'Mohon maaf, pembaruan mata kuliah gagal dengan error: ' . $e->getMessage());
            }
        }
        return redirect(route('mata-kuliah'))->with('message', 'Mata kuliah ' . $mataKuliah->nama . ' berhasil diperbarui.');
    }

    /**
     * Fungsi untuk menghapus sebuah Mata Kuliah.
     *
     * @param HapusMatkulRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusMatkul(HapusMatkulRequest $request) {
        $validated = $request->validated();
        $nama = " ";
        try {
            $mataKuliah = MataKuliah::find($validated['id']);
            $nama = $nama . $mataKuliah->nama . " ";
            $mataKuliah->delete();
        } catch (Throwable $e) {
            return redirect(route('mata-kuliah'))->with('message', 'Mohon maaf, mata kuliah' . $nama . 'gagal dihapus dengan error: ' . $e->getMessage());
        }
        return redirect(route('mata-kuliah'))->with('message', 'Mata kuliah' . $nama . 'berhasil dihapus.');
    }

    /**
     * Fungsi untuk menambahkan sebuah Mata Kuliah Ditawarkan.
     *
     * @param \Modules\MataKuliahDitawarkan\Requests\Store $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tambahkanMatkulDitawarkan(\Modules\MataKuliahDitawarkan\Requests\Store $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $matkulDitawarkan = new MataKuliahDitawarkan;
        try {
            $matkulDitawarkan->id = DB::table('mata_kuliah_ditawarkan')->find(DB::table('mata_kuliah_ditawarkan')->max('id'))->id + 1;
            $matkulDitawarkan->mata_kuliah_id = $validated['mata_kuliah_id'];
            $matkulDitawarkan->dosen_id = $validated['dosen_id'];
            $matkulDitawarkan->ruang_kelas_id = $validated['ruang_kelas_id'];
            $matkulDitawarkan->save();
        } catch (Throwable $e) {
            return redirect(route('matkul-ditawarkan'))->with('message', 'Mohon maaf, penambahan mata kuliah ditawarkan terjadi error: ' . $e->getMessage());
        }
        return redirect(route('matkul-ditawarkan'))->with('message', 'Mata kuliah ditawarkan berhasil ditambahkan.');
    }

    /**
     * Fungsi untuk memperbarui sebuah Mata Kuliah Ditawarkan.
     *
     * @param PerbaruiMatkulDitawarkanRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiMatkulDitawarkan(PerbaruiMatkulDitawarkanRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $matkulDitawarkan = MataKuliahDitawarkan::find($validated['id']);
            if (!empty($validated['mata_kuliah_id'])) {
                $matkulDitawarkan->mata_kuliah_id = strip_tags($validated['mata_kuliah_id']);
            }
            if (!empty($validated['dosen_id'])) {
                $matkulDitawarkan->dosen_id = $validated['dosen_id'];
            }
            if (!empty($validated['ruang_kelas_id'])) {
                $matkulDitawarkan->ruang_kelas_id = $validated['ruang_kelas_id'];
            }
            $matkulDitawarkan->save();
        } catch (Throwable $e) {
            return redirect(route('matkul-ditwarkan'))->with('message', 'Mohon maaf, pembaruan mata kuliah ditawarkan gagal dengan error: ' . $e->getMessage());
        }
        return redirect(route('matkul-ditawarkan'))->with('message', 'Mata kuliah ditawarkan berhasil diperbarui.');
    }

    /** Fungsi untuk menghapus sebuah Mata Kuliah Ditawarkan.
     *
     * @param HapusMatkulDitawarkanRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusMatkulDitawarkan(HapusMatkulDitawarkanRequest $request) {
        $validated = $request->validated();
        try {
            $mataKuliahDitawarkan = MataKuliahDitawarkan::find($validated['id']);
            $mataKuliahDitawarkan->delete();
        } catch (Throwable $e) {
            return redirect(route('matkul-ditawarkan'))->with('message', 'Mohon maaf, mata kuliah ditawarkan gagal dihapus dengan error: ' . $e->getMessage());
        }
        return redirect(route('matkul-ditawarkan'))->with('message', 'Mata kuliah ditawarkan berhasil dihapus.');
    }
}
