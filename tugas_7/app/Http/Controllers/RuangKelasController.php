<?php

namespace App\Http\Controllers;

use App\Http\Requests\HapusRuangRequest;
use App\Http\Requests\PerbaruiRuangRequest;
use Illuminate\Support\Facades\DB;
use Modules\RuangKelas\Models\RuangKelas;
use Modules\RuangKelas\Requests\Store;
use Throwable;

class RuangKelasController extends Controller
{
    /**
     * Fungsi untuk mengembalikan view Daftar Ruang Kelas.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ruangKelas() {
        try {
            $ruangKelasCollection = RuangKelas::all();
        } catch (Throwable $e) {
            return view('/')->with('message', 'Mohon maaf, terjadi error dalam memuat ruang kelas: ' . $e->getMessage());
        }
        return view('ruang-kelas', compact('ruangKelasCollection'));
    }

    /**
     * Fungsi untuk menambahkan sebuah Ruang Kelas.
     *
     * @param Store $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tambahkanRuang(Store $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $ruangKelas = new RuangKelas;
        try {
            $ruangKelas->id = DB::table('ruang_kelas')->find(DB::table('ruang_kelas')->max('id'))->id + 1;
            $ruangKelas->nama = strip_tags($validated['nama']);
            $ruangKelas->save();
        } catch (Throwable $e) {
            return redirect(route('ruang-kelas'))->with('message', 'Mohon maaf, penambahan ruang kelas ' . $validated['nama'] . ' terjadi error: ' . $e->getMessage());
        }
        return redirect(route('ruang-kelas'))->with('message', 'Ruang kelas ' . $validated['nama'] . ' berhasil ditambahkan.');
    }

    /**
     * Fungsi untuk memperbarui sebuah Ruang Kelas
     *
     * @param PerbaruiRuangRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiRuang(PerbaruiRuangRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $ruangKelas = RuangKelas::find($validated['id']);
            $ruangKelas->nama = strip_tags($validated['nama']);
            $ruangKelas->save();
        } catch (Throwable $e) {
            if (isset($ruangKelas)) {
                return redirect(route('ruang-kelas'))->with('message', 'Mohon maaf, pembaruan ruang kelas' . $ruangKelas->nama . ' gagal dengan error: ' . $e->getMessage());
            } else {
                return redirect(route('ruang-kelas'))->with('message', 'Mohon maaf, pembaruan ruang kelas gagal dengan error: ' . $e->getMessage());
            }
        }
        return redirect(route('ruang-kelas'))->with('message', 'Ruang kelas ' . $ruangKelas->nama . ' berhasil diperbarui.');
    }

    /**
     * Fungsi untuk menghapus sebuah Ruang Kelas.
     *
     * @param HapusRuangRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusRuang(HapusRuangRequest $request) {
        $validated = $request->validated();
        $namaRuang = " ";
        try {
            $ruangKelas = RuangKelas::find($validated['id']);
            $namaRuang = $namaRuang . $ruangKelas->nama . " ";
            $ruangKelas->delete();
        } catch (Throwable $e) {
            return redirect(route('ruang-kelas'))->with('message', 'Mohon maaf, ruang kelas' . $namaRuang . 'gagal dihapus dengan error: ' . $e->getMessage());
        }
        return redirect(route('ruang-kelas'))->with('message', 'Ruang kelas' . $namaRuang . 'berhasil dihapus.');
    }
}
