<?php

namespace App\Http\Controllers;

use App\Http\Requests\AmbilMatkulRequest;
use App\Http\Requests\BatalkanPengambilanRequest;
use App\Models\Krs;
use Modules\Mahasiswa\Models\Mahasiswa;
use Modules\MataKuliahDitawarkan\Models\MataKuliahDitawarkan;
use Throwable;

class KrsController extends Controller
{
    /**
     * Fungsi untuk mengembalikan view KRS.
     *
     * @param $mahasiswaId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function krs($mahasiswaId) {
        if (!is_numeric($mahasiswaId)) {
            return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, tautan krs salah.');
        }
        try {
            $mahasiswa = Mahasiswa::find($mahasiswaId);
            if ($mahasiswa != null) {
                $matkulDitawarkanCollection = MataKuliahDitawarkan::all();
                $krs = $mahasiswa->krs;
            }
        } catch (Throwable $e) {
            return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, terjadi error dengan pesan: ' . $e->getMessage());
        }
        if ($mahasiswa == null) {
            return redirect(route('daftar-mahasiswa'))->with('message', 'Mohon maaf, tautan krs salah.');
        }
        return view('krs', compact('mahasiswa', 'matkulDitawarkanCollection', 'krs'));
    }

    /**
     * Fungsi untuk pengambilan mata kuliah KRS.
     *
     * @param AmbilMatkulRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ambilMatkul(AmbilMatkulRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $krs = new Krs;
        try {
            $krs->mahasiswa_id = $validated['mahasiswa_id'];
            $krs->mata_kuliah_ditawarkan_id = $validated['mata_kuliah_ditawarkan_id'];
            $krs->save();
        } catch (Throwable $e) {
            return redirect(route('krs', ['mahasiswaId' => $validated['mahasiswa_id']]))->with('message', 'Mohon maaf, pengambilan mata kuliah terjadi error: ' . $e->getMessage());
        }
        return redirect(route('krs', ['mahasiswaId' => $validated['mahasiswa_id']]))->with('message', 'Matkul berhasil diambil.');
    }

    /**
     * Fungsi untuk pembatalan pengambilan mata kuliah dalam KRS.
     *
     * @param BatalkanPengambilanRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function batalkanPengambilan(BatalkanPengambilanRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        $krs = Krs::find($validated['id']);
        try {
            $krs->delete();
        } catch (Throwable $e) {
            return redirect(route('krs', ['mahasiswaId' => $validated['mahasiswa_id']]))->with('message', 'Mohon maaf, pembatalan pengambilan mata kuliah terjadi error: ' . $e->getMessage());
        }
        return redirect(route('krs', ['mahasiswaId' => $validated['mahasiswa_id']]))->with('message', 'Pembatalan pengambilan mata kuliah berhasil.');
    }
}
