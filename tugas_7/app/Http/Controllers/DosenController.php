<?php

namespace App\Http\Controllers;

use App\Http\Requests\HapusDosenRequest;
use App\Http\Requests\PerbaruiDosenRequest;
use App\Models\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class DosenController extends Controller
{
    /**
     * Mengembalikan view daftar dosen.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function daftarDosen() {
        $dosenCollection = Dosen::all();
        return view('daftar-dosen', compact('dosenCollection'));
    }

    /**
     * Menambahkan data seorang dosen.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function tambahkanDosen(Request $request) {
        // memastikan bahwa parameter-parameter yang dimasukkan valid
        $rules = [
            'nama' => 'required|max:255',
            'nip' => 'required|max:255',
            'gelar' => 'required|max:255'
        ];
        $this->validate($request, $rules);
        $dosen = new Dosen;
        try {
            $dosen->id = DB::table('dosen')->find(DB::table('dosen')->max('id'))->id + 1;
            $dosen->nama = strip_tags($request->nama);
            $dosen->nip = strip_tags($request->nip);
            $dosen->gelar = strip_tags($request->gelar);
            $dosen->save();
        } catch (Throwable $e) {
            return redirect('/daftar-dosen')->with('message', 'Mohon maaf, penambahan dosen ' . $request->nama . ', ' . $request->gelar . ' terjadi error: ' . $e->getMessage());
        }
        return redirect('/daftar-dosen')->with('message', 'Dosen ' . $request->nama . ', ' . $request->gelar . ' berhasil ditambahkan.');
    }

    /**
     * Memperbarui data seorang dosen.
     *
     * @param PerbaruiDosenRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function perbaruiDosen(PerbaruiDosenRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $dosen = Dosen::find($validated['id']);
            if (!empty($validated['nama'])) {
                $dosen->nama = strip_tags($validated['nama']);
            }
            if (!empty($validated['nip'])) {
                $dosen->nip = strip_tags($validated['nip']);
            }
            if (!empty($validated['gelar'])) {
                $dosen->gelar = strip_tags($validated['gelar']);
            }
            $dosen->save();
        } catch (Throwable $e) {
            if (isset($dosen)) {
                return redirect('/daftar-dosen')->with('message', 'Mohon maaf, pembaruan data dosen ' . $dosen->nama . ', ' . $dosen->gelar . ' gagal dengan error: ' . $e->getMessage());
            } else {
                return redirect('/daftar-dosen')->with('message', 'Mohon maaf, pembaruan data dosen gagal dengan error: ' . $e->getMessage());
            }
        }
        return redirect('/daftar-dosen')->with('message', 'Dosen ' . $dosen->nama . ', ' . $dosen->gelar . ' berhasil diperbarui.');
    }

    /**
     * Menghapus data seorang dosen.
     *
     * @param HapusDosenRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function hapusDosen(HapusDosenRequest $request) {
        // Mendapatkan masukan data tervalidasi
        $validated = $request->validated();
        try {
            $dosen = Dosen::find($validated['id']);
            $nama = $dosen->nama;
            $gelar = $dosen->gelar;
            $dosen->delete();
        } catch (Throwable $e) {
            if (isset($nama) && isset($gelar)){
                return redirect('/daftar-dosen')->with('message', 'Dosen ' . $nama . ', ' . $gelar . ' gagal dihapus dengan error: ' . $e->getMessage());
            } elseif (isset($nama)) {
                return redirect('/daftar-dosen')->with('message', 'Dosen ' . $nama . ' gagal dihapus dengan error: ' . $e->getMessage());
            } else {
                return redirect('/daftar-dosen')->with('message', 'Dosen gagal dihapus dengan error: ' . $e->getMessage());
            }
        }
        return redirect('/daftar-dosen')->with('message', 'Dosen ' . $nama . ' berhasil dihapus.');
    }
}
