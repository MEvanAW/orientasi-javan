<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerbaruiMahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasikan ACL
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'numeric', 'exists:mahasiswa'],
            'nama' => ['nullable', 'max:255'],
            'nim' => ['nullable', 'max:255'],
            'jenis_kelamin' => ['nullable', 'max:10'],
            'tempat_lahir' => ['nullable', 'max:255'],
            'tanggal_lahir' => ['nullable', 'date', 'before:' . date("Y-m-d")]
        ];
    }
}
