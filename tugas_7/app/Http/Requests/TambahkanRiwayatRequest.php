<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TambahkanRiwayatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasi ACL
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'strata' => 'required|max:255',
            'jurusan' => 'required|max:255',
            'sekolah' => 'required|max:255',
            'tahun_mulai' => 'required|numeric|max:' . date("Y"),
            'tahun_selesai' => 'required|numeric|max:' . date("Y")
        ];
    }
}
