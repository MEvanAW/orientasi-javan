<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HapusRiwayatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasi ACL
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:riwayat_pendidikan',
            'dosen_id' => 'required|numeric|exists:dosen,id'
        ];
    }
}
