<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BatalkanPengambilanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasikan ACL
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mahasiswa_id' => 'required|numeric|exists:mahasiswa,id',
            'id' => 'required|numeric|exists:krs'
        ];
    }
}
