<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerbaruiRiwayatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Implementasi ACL
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:riwayat_pendidikan',
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'strata' => 'nullable|max:255',
            'jurusan' => 'nullable|max:255',
            'sekolah' => 'nullable|max:255',
            'tahun_mulai' => 'nullable|numeric|max:' . date("Y"),
            'tahun_selesai' => 'nullable|numeric|max:' . date("Y")
        ];
    }
}
